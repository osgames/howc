#!/usr/bin/python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, pygame, re, os
from pygame.locals import *
import defines as WC
import window as SWINDOW
import widgets as SWIDGET
import keys as SKEY
import events as SEVENT
import sound as SSFX
import data as SDATA

# class that holds the dirty rectangle updates
class CDirtyRect(object):
	def __init__(self, pic, rec):
		self.image = pic
		self.rect = rec
	
	def update(self, image):
		"""Update sent image"""
		image.blit(self.image, self.rect)

# now of course we need a class to hold all of the windows, i.e. the basic GUI class
# this class also inits the gfx display
# call with the x and y resolution of the screen, and a pointer to the data
class CGFXEngine(object):
	def __init__(self):
		""" init the gui screen and widgets and any other data we need
			to keep track for the graphic engine """
		# ok, now init the basic screen
		pygame.init()
		pygame.key.set_repeat(100,40)
		#pygame.display.set_icon(pygame.image.load("./howc.ico"))
		self.images = {}
		self.windows = []
		# Timer and interrupts
		self.clock =  pygame.time.Clock()
		self.timer = True
		self.tick = True
		# animation handlers
		self.rotation = 0
		# item to check double-click against
		self.dclick_handle = None
		
		# Set up the fonts
		pygame.font.init()
		self.fonts = []
		self.fonts.append(pygame.font.Font("./gfx/Vera.ttf", WC.FONT_STD))
		self.fonts.append(pygame.font.Font("./gfx/Vera.ttf", WC.FONT_SMALL))
		self.fonts.append(pygame.font.Font("./gfx/Vera.ttf", WC.FONT_LARGE))
		self.fonts.append(pygame.font.Font("./gfx/Vera-Bold.ttf", WC.FONT_STD))
		self.fonts.append(pygame.font.Font("./gfx/Vera-Italic.ttf", WC.FONT_STD))

		# modal windows use a dirty rect list to update, here it is
		self.dirty = []
		# hex dirty images from highlighting moves
		self.nav_dirty = []
		# here is the hints dirty rect list
		self.hint = None
		self.current_hint = None
		# enable keyboard reponses
		self.keyboard = SKEY.CKeyboard()
		# variables so callbacks and external code can communicate
		self.callback_temp = WC.BUTTON_FAIL
		# a flag to see if a menu is waiting for input
		self.menu_active = False

	def mainInit(self, gfx_folder):
		""" Long, boring routine that initiates the gui for the game. The gfx_folder flag
		    shows what gfx folders to load to the engine """
		# widget that catches all the calls for the map
		self.map_widget = None
		# Mini map widget
		self.minimap_widget = None
		# region control widgets. First the dirty rect for the highlighted
		# region and then the current highlighted region class object
		self.region_highlight = None
		self.current_highlight_region = None
		self.current_selected_unit = None
		# button widgets for system control and board map control
		self.main_map_widgets = []
		self.board_map_widgets = []
		# unit info,animation and movement data
		self.info_widget = None
		self.board_move_list = None
		# list to hold possible moves being shown
		self.map_click_moves = []
		# flag to show if we are inside a region or not
		self.board = False
		# index for the action window
		self.action_window = None
		
		######################################################################
		# SOS this is the most Important initiation we don't have in _init_  #
		# Check if we have fullscreen and initiate the pygame screen		 #
		######################################################################
		if WC.FULLSCREEN:
			display = pygame.display.Info()
			WC.SCREEN_WIDTH, WC.SCREEN_HEIGHT = display.current_w, display.current_h
			self.screen = pygame.display.set_mode((WC.SCREEN_WIDTH, WC.SCREEN_HEIGHT),
				HWSURFACE|FULLSCREEN|DOUBLEBUF)
		else:
			self.screen = pygame.display.set_mode((WC.SCREEN_WIDTH, WC.SCREEN_HEIGHT), HWSURFACE|DOUBLEBUF)
		# Set the program caption for the main window
		pygame.display.set_caption("Heroes of Wing Commander " + WC.VERSION)
		######################################################################
		
		# Set the program icon for the main window
		if os.path.isfile("icon"):
			pygame.display.set_icon(pygame.image.load(WC.ICON))
		
		# Load loading screen and play intro
		if WC.LOADSCREEN == True:
			self.displayLoadingScreen(WC.SCREEN_WIDTH, WC.SCREEN_HEIGHT)
		if WC.INTRO:
			print " must play intro "
		# get all graphic filenames:
		files = []
		WC.GRAPHICS_F = gfx_folder
		for i in WC.GRAPHICS_F:
			files.extend(["./gfx/" + i +"/" + name for name in os.listdir("./gfx/" + i + "/")])
		# if it's a png, strip the name and insert it into a new hash
		for i in files:
			if i[-4:] == ".png":
				self.images[i.split("/")[-1][:-4]] = pygame.image.load(i).convert_alpha()
			elif i[-4:] == ".jpg":
				self.images[i.split("/")[-1][:-4]] = pygame.image.load(i)

		# where to start the map blit from when blasting it to the screen
		menu_y_max = (WC.SCREEN_HEIGHT - self.iHeight("win_tl")) + 1
		# define the 'from' rectangle
		self.map_screen = pygame.Rect((0, 0, WC.SCREEN_WIDTH, menu_y_max))
		# and the target rectangle for the blit:
		self.map_rect = pygame.Rect((0, (self.iHeight("win_tl"))-1,
			WC.SCREEN_WIDTH, menu_y_max))
		# area that the map covers on the screen:
		self.map_area = pygame.Rect((0, self.iHeight("win_tl"),
			WC.SCREEN_WIDTH, (WC.SCREEN_HEIGHT-self.iHeight("win_tl"))))

		# Mini map variables
		self.blit_rect = pygame.Rect(0, 0, 0, 0)

		# add a back buffer map render.. this will become the map that we render
		# we initialize the buffer with the main map
		self.images["map"] = pygame.Surface((WC.SCREEN_WIDTH, WC.SCREEN_HEIGHT))
		self.images["map"].fill(WC.BGUI_COL)
		self.loadMiniMap(None)
		self.images["buffer"] = self.images["map"].copy()
		
		# store a rect of the maximum map limits we can scroll to
		# obviously 0/0 for top left corner - this just denotes bottom right corner
		self.map_max_x = self.iWidth("map") - WC.SCREEN_WIDTH
		self.map_max_y = self.iHeight("map") - self.map_rect.h
		
		# centre the map for the start blit.
		self.centerMap(-1, -1)

		# start the first song here, if music is enabled
		if WC.MUSIC_ON == True: SSFX.sound.startNextSong()
		
	def loadMiniMap(self, filename):
		""" Re initiate the rectangle and size of the mini map widget"""
		if filename == None:
			self.images["small_map"] = pygame.Surface((WC.SMALL_MAP_W, WC.SMALL_MAP_H))
			self.images["small_map"].fill(WC.COLG_RED)
		else:
			self.images["small_map"] = pygame.image.load(filename).convert_alpha()
			# Rectangle values
			self.minimap_widget.rect.w = self.iWidth("small_map")
			self.minimap_widget.rect.h = self.iHeight("small_map")
			self.minimap_widget.rect.x = WC.SCREEN_WIDTH - (self.iWidth("small_map") + WC.SPACER)
			self.minimap_widget.rect.y = WC.SCREEN_HEIGHT - (self.iHeight("small_map") + (2*WC.SPACER) + 1 + self.iHeight("titlebar"))
		# calculate width and height of square to blit
		self.width_ratio = float(self.iWidth("map")) / float(self.iWidth("small_map") - 2)
		self.height_ratio = float(self.iHeight("map")) / float(self.iHeight("small_map") - 2)
		# LESSON: in python, you need to force the floats sometimes
		self.blit_rect.w = int(float(self.map_rect.w) / self.width_ratio)
		self.blit_rect.h = int(float(self.map_rect.h) / self.height_ratio)

	def displayLoadingScreen(self, width, height):
		"""Displays the loading screen"""
		load_screen = pygame.image.load("./gfx/load_screen.png").convert()
		self.screen.fill(WC.COL_BLACK)
		xpos = (width - load_screen.get_width()) / 2
		ypos = (height - load_screen.get_height()) / 2
		self.screen.blit(load_screen, (xpos, ypos))
		pygame.display.update()

	def iWidth(self, name):
		return self.images[name].get_width()
	
	def iHeight(self, name):
		return self.images[name].get_height()
	
	def image(self, name):
		return self.images[name]

	def addWindow(self, window):
		"""Call to add a window to the gui window list. It always goes
		   on the top of the window pile, and thus if modal traps all
		   user input. Returns the index number of the window in the list,
		   so you can amend the window afterwards"""
		self.windows.append(window)
		# since we always append to the list, the index is always
		# the size of the array minus 1 (since we start the array at 0)
		index = len(self.windows) - 1
		return index
	
	def addDirtyRect(self, new, rectangle):
		"""Routine adds dirty rectangle and details to the current list"""
		# get the old image from the screen
		img = pygame.Surface((rectangle.w, rectangle.h))
		img.blit(pygame.display.get_surface(), (0, 0), rectangle)
		# now blit the new image
		self.screen.blit(new, (rectangle.x, rectangle.y))
		pygame.display.update()
		self.dirty.append(CDirtyRect(img, rectangle))
		return True

	def deleteTopDirty(self):
		"""Routine deletes current top dirty window, and draws
		   back the old image"""
		# actually got something to do?
		if len(self.dirty) > 0:
			# yes, update the screen first
			self.screen.blit(self.dirty[-1].image, self.dirty[-1].rect)
			pygame.display.update(self.dirty[-1].rect)
			self.dirty.pop()
		else:
			return False
		return True
	
	# TODO: Sort out wether we need to really update the gui here
	# generally used to kill the active window
	def killIndexedWindow(self, index):
		"""Remove window. Call with index number of window. Redraws
		   gui as well"""
		del self.windows[index]
		return True
		
	def killActionWindow(self):
		""" Remove action window based on index """
		if self.action_window != None:
			self.killIndexedWindow(self.action_window)
			self.action_window = None
	
	def killTopWindow(self):
		"""Remove top window. Redraws gui as well"""
		self.windows.pop()
		return True

	def updateGUI(self):
		"""Redraws entire screen. Should avoid calling this really and use
		   dirty rectangles technique. Having said that, it's not actually
		   that slow either"""
		# if we have anything in the dirty list, we merely have to update that area
		# with the new image etc...
		if len(self.dirty) > 0:
			self.screen.blit(self.dirty[-1].image, self.dirty[-1].rect)
			pygame.display.update(self.dirty[-1].rect)
			return True
		# before doing anything else, blit the map
		self.screen.blit(self.image("buffer"), self.map_rect, self.map_screen)
		index = 0
		# we have to do the window testing in reverse to the way we blit, as the first
		# object blitted is on the 'bottom' of the screen, and we have to test from the top
		for window in self.windows:
			if window.display:
				self.screen.blit(window.image, (window.rect.x, window.rect.y))
			for item in window.items:
				if item.visible:
					# is this the mini-map?
					if item.describe  ==  "mini-map":
						# just update it
						self.updateMiniMap()
						x1 = window.rect.x + item.rect.x
						y1 = window.rect.y + item.rect.y
						self.screen.blit(item.image, (x1, y1))
					else:
						x1 = window.rect.x + item.rect.x
						y1 = window.rect.y + item.rect.y
						self.screen.blit(item.image, (x1, y1))
			index += 1
		pygame.display.flip()
		return True
	
	def updateOverlayWindow(self, index = -1):
		"""Draw the window with the widget overlays only"""
		# TODO: a better way of finding the window
		offset = self.windows[index].rect
		for i in self.windows[index].items:
			if i.visible:
				self.screen.blit(i.image, (i.rect.x+offset.x, i.rect.y+offset.y))

	# this one merely updates the map, rather than blit all those
	# gui things as well
	def updateMap(self, flip = True):
		"""Updates (i.e. redraws) map to main screen"""
		self.screen.blit(self.image("buffer"), self.map_rect, self.map_screen)
		self.updateOverlayWindow()
		if flip:
			pygame.display.flip()

	# and this one merely blits the cursor in the mini map
	def updateMiniMap(self):
		"""Redraws mini-map, usually called after any changes to
		   the map on the main screen"""
		# work out what the corrent co-ords are for the mini-map cursor
		xpos = int(self.map_screen.x / self.width_ratio)
		ypos = int(self.map_screen.y / self.height_ratio)
		self.blit_rect.x = xpos + 1
		self.blit_rect.y = ypos + 1
		if self.minimap_widget != None:
			self.minimap_widget.image = self.images["small_map"].copy()
			# draw a border for the mini map with black color
			pygame.draw.rect(self.minimap_widget.image, WC.COL_WHITE, self.blit_rect, 1)
		return True

	def centerMap(self, xpos, ypos):
		"""Centre map to the given co-ords, or at least as close as you we
		   get. DOES NOT update the screen for you"""
		# firstly, rectify the co-ords so that they will be in the
		# centre of the screen
		xpos -= self.map_screen.w/2
		ypos -= self.map_screen.h/2
		if xpos < 0:
			xpos = 0
		elif xpos > (self.iWidth("buffer") - self.map_screen.w):
			xpos = self.iWidth("buffer") - self.map_screen.w
		# and then check y size
		if ypos < 0:
			ypos = 0
		elif ypos > (self.iHeight("buffer") - self.map_screen.h):
			ypos = self.iWidth("buffer") - self.map_screen.h
		# set new co-ords
		self.map_screen.x = xpos
		self.map_screen.y = ypos
		return True

	def normalizeScrollArea(self):
		"""Checks co-ords after scrolling the map to make sure
		   they are not out of range. Resets them if needed"""
		if self.map_screen.x < 0:
			self.map_screen.x = 0
		elif self.map_screen.x > self.map_max_x:
			self.map_screen.x = self.map_max_x
		if self.map_screen.y < 0:
			self.map_screen.y = 0
		elif self.map_screen.y > self.map_max_y:
			self.map_screen.y = self.map_max_y
		# update action window
		if self.action_window != None: self.killActionWindow()
		return True
	
	def handleKeypress(self, event):
		"""Handle a keypress"""
		# does it match anything?
		foo, bar, handle = self.keyboard.getKeyFunction(event.key, event.mod)
		if foo:
			# set win_index to TOP of current window list -2 to enable
			# killing of current window from keyboard function
			self.win_index = len(self.windows)-2
			# now call the function
			if handle == None:
				bar(0, -1, -1)
			else:
				bar(handle, -1, -1)
			return True
		else:
			return False
	
	# routine captures what event we got, then passes that message along
	# to the testing routine (i.e. this code only checks if a MOUSE event
	# happened, the later function checks if we got a GUI event)
	def checkInputs(self, event):
		"""checkInputs() is called on a loop whilst the game is waiting
		   for user input (i.e. most of the time). It doesn't actually do
		   anything with the input except pass the event along to somewhere
		   else, so think of it more like a sorting office"""
		# lets start with the simple case: handling keypress values
		if event.type == KEYDOWN:
			# Erase any hint on screen
			self.killHint()
			return self.handleKeypress(event)
		# now handle animation requests from the timer
		if event.type == pygame.USEREVENT and self.timer:
			###############################
			# TODO: Animation timer calls #
			###############################
			self.rotation += 1
			return False
		action = WC.MOUSE_NONE
		# catch other stuff here, before we process the mouse
		# perhaps it was just that the song ended?
		if event.type == WC.EVENT_SONGEND:
			# just start the next song
			SSFX.sound.startNextSong()
			return True
		# was it the end of a double-click check?
		if event.type == WC.EVENT_DC_END:
			# kill timer and handle data
			pygame.time.set_timer(WC.EVENT_DC_END, 0)
			self.dclick_handle = None
			return True
		# Time event for keeping track of time
		if event.type == WC.EVENT_TIME:
			# return the miliseconds
			self.tick=False
			return True
		# worst of all, could be an instant quit!
		if event.type == pygame.QUIT:
			SEVENT.quitWC(None, -1, -1)
			return True
		# cancel current menu if we got mouse button down
		if event.type == MOUSEBUTTONDOWN and self.menu_active:
			self.menu_active = False
			return False
		if event.type != NOEVENT:
			# Check and erase any hint on screen
			self.killHint()
			# if it's a rmb down, then possibly exit
			if event.type == MOUSEBUTTONDOWN and event.button == 3:
				if WC.RMOUSE_END:
					sys.exit(False)
				else:
					x, y = pygame.mouse.get_pos()
					action = WC.MOUSE_RCLK
					self.testMouse(x, y, action)
			# was it left mouse button up?
			elif event.type == MOUSEBUTTONUP and event.button == 1:
				x, y = pygame.mouse.get_pos()
				action = WC.MOUSE_LCLK
				self.testMouse(x, y, action)
			# some things (like sliders) respond to a mousedown event
			elif event.type == MOUSEBUTTONDOWN and event.button == 1:
				x, y = pygame.mouse.get_pos()
				action = WC.MOUSE_LDOWN
				self.testMouse(x, y, action)
			# was it a middle click?
			elif event.type == MOUSEBUTTONDOWN and event.button == 2:
				# pan the map, unless we have a modal window:
				if self.windows[len(self.windows) - 1].modal == False:
					# must be over main map for panning to work
					x, y = pygame.mouse.get_pos()
					if self.map_area.collidepoint(x, y):
						self.panMap()
			else:
				# have we moved?
				if event.type == MOUSEMOTION:
					x, y = pygame.mouse.get_pos()
					action = WC.MOUSE_OVER
					if self.testMouse(x, y, action) == False:
						self.checkButtonHighlights(x, y)
			if action == WC.MOUSE_NONE:
				return False
			else:	
				return True

	def checkButtonHighlights(self, x, y):	
		"""Check all of the buttons inside the top-layer window to see
		   if any need to be highlighted. Returns True if anything
		   on the screen needed to be updated"""
		for bar in self.windows[-1].items:
			if bar.active and bar.wtype == WC.WT_BUTTON:
				xoff = x-self.windows[-1].rect.x
				yoff = y-self.windows[-1].rect.y
				if bar.rect.collidepoint(xoff, yoff):
					# don't forget to test here if it's actually visible or not... ;-)
					if bar.visible == False:
						return False
					# already highlighted?
					if bar.highlight:
						return False
					else:
						# update a dirty rect
						bar.highlight = True
						dest = pygame.Rect(bar.rect.x+self.windows[-1].rect.x,
							bar.rect.y+self.windows[-1].rect.y, bar.rect.w, bar.rect.h)
						self.screen.blit(bar.pressed, dest)
						pygame.display.update(dest)
						return True
				if bar.highlight and bar.wtype == WC.WT_BUTTON:
					# an old highlight needs rubbing out
					bar.highlight = False
					dest = pygame.Rect(bar.rect.x+self.windows[-1].rect.x,
						bar.rect.y+self.windows[-1].rect.y, bar.rect.w, bar.rect.h)
					self.screen.blit(bar.image, dest)
					pygame.display.update(dest)
					return True
		return False

	# use this function to test the mouse against all objects
	def testMouse(self, x, y, action):
		"""testMouse returns False if nothing got called
		   Otherwise it handles checking the action against all
		   of the widgets, menus and windows that are active"""
		quit = False
		# normally I'd use for foo in self.windows, but we need to traverse
		# this list in the opposite direction to the way we render them
		self.win_index = len(self.windows)-1
		# There is no window present, is there a mouse down onto the map
		while(self.win_index>-1):
			# define a new variable that we can use later to kill the current window off
			foo = self.windows[self.win_index]			
			self.win_index = self.win_index-1
			if quit:
				return False
			# if this is a modal window, then stop after processing:
			quit = foo.modal
			# is the mouse pointer inside the window, or is there any window at all?
			if foo.rect.collidepoint(x, y) or foo.display == False:
				# check all of the points inside the window
				for bar in foo.items:
					if bar.active:
						x_off = x - foo.rect.x
						y_off = y - foo.rect.y
						if bar.rect.collidepoint(x_off, y_off):
							# get offset into widget
							x_widget = x_off-bar.rect.x
							y_widget = y_off-bar.rect.y
							# now test to see if we need to make a call
							if action == WC.MOUSE_OVER and bar.callbacks.mouse_over != WC.mouse_over_std:
								# widget asked for callback on mouse over
								bar.callbacks.mouse_over(bar, x_widget, y_widget)
								return True
							elif action == WC.MOUSE_LCLK and bar.callbacks.mouse_lclk != WC.mouse_lclk_std:
								# widget asked for callback on mouse left click								
								bar.callbacks.mouse_lclk(bar, x_widget, y_widget)
								return True
							elif action == WC.MOUSE_LCLK and \
									 bar.callbacks.mouse_dclick != WC.mouse_dclick_std and \
									 self.dclick_handle != bar:
								# widget wants a double-click: this was the first one, so we need
								# to keep an eye out for the next click
								self.dclick_handle = bar
								# set our timer
								pygame.time.set_timer(WC.EVENT_DC_END, WC.DCLICK_SPEED)
								return False
							elif action == WC.MOUSE_LCLK and \
									bar.callbacks.mouse_dclick != WC.mouse_dclick_std and \
									self.dclick_handle == bar:
								# it's a real bona-fida double-click
								# firstly clear all double-click data, then run the code
								pygame.time.set_timer(WC.EVENT_DC_END, 0)
								self.dclick_handle = None
								bar.callbacks.mouse_dclick(bar, x_widget, y_widget)
								return True
							elif action == WC.MOUSE_DCLICK:
								# unwanted double-click
								pygame.time.set_timer(WC.EVENT_DC_END, 0)
								self.dclick_handle = None
								return False
							elif action == WC.MOUSE_LDOWN and bar.callbacks.mouse_ldown != WC.mouse_ldown_std:
								# widget asked for callback on mouse left down
								bar.callbacks.mouse_ldown(bar, x_widget, y_widget)
								return True
							elif action == WC.MOUSE_RCLK and bar.callbacks.mouse_rclk != WC.mouse_rclk_std:
								bar.callbacks.mouse_rclk(bar, x_widget, y_widget)
								return True
							# and then exit
							return False
		return False

	def screenToMapCoords(self, x, y):
		"""Convert screen click co-ords to map co-ords"""
		x += self.map_screen.x
		y -= self.iHeight("titlebar")
		y += self.map_screen.y
		return x, y
	
	def mapToScreenCoords(self, x, y):
		"""Convert map co-ords to screen click co-ords"""
		x -= self.map_screen.x
		y += self.iHeight("titlebar")
		y -= self.map_screen.y
		return x, y
	
	def mapHint(self, handle, xpos, ypos):
		""" Call with x and y, being the mouse over on
			the map in screen co-ords"""
		# we are inside the main widget, so offset the y position
		ypos += self.iHeight("titlebar")
		x, y = self.screenToMapCoords(xpos, ypos)
		# check if the main map is hexed and set the hex cursor
		if self.board == False:
			# TODO: Hint for region/poi main map
			self.current_hint = SDATA.regionClicked(x, y)
	
	def killHint(self):
		""" removes the last hint on a map or widget """
		if self.hint != None:
			self.screen.blit(self.hint.image, self.hint.rect)
			pygame.display.update(self.hint.rect)
			self.hint = None

	def mapClick(self, handle, xpos, ypos):
		"""Call with x and y, being the click on the map
		   in screen co-ords"""
		# we are inside the main widget, so offset the y position
		ypos += self.iHeight("titlebar")
		x, y = self.screenToMapCoords(xpos, ypos)
		# Checking if we are in main map or board map
		if self.board == False:
			# find the region of the given cords or return None
			r = SDATA.regionClicked(x, y)
			self.mainMapClick(r, x, y)
		else:
			# finding hex clicked
			r = SDATA.hexRegionClicked(x, y)
			self.boardMapClick(r)
		return True
	
	def boardMapClick(self, nav):
		""" checks for gui hex clicks on board map """
		# check if there is a movement click
		if self.board_move_list != None:
			src, mlist = self.board_move_list
			# clear the movment choises
			self.board_move_list = None
			# clear all highlighted hexes
			self.highlightNav([])
			# check if we clicked a highlighted hex to move a unit to
			if nav == src: print "No movement"
			elif nav in mlist.keys():
				# move the unit
				if self.unitAnim(src, nav) == False:
					# check what actions are availiable
					self.checkAction(src.unit, nav.unit)
				else: print "Move OK"
			self.updateMap()
			return True
		# if we have something on the hex
		if nav.unit != None:
			# debug info on unit
			if WC.DEBUG_MODE == True: nav.unit.printStats()
			# clear action window 
			self.killActionWindow()
			# unit selection
			self.current_selected_unit = nav.unit
			# check if this is our unit or nav
			if nav.unit.owner == SDATA.data.players["Player"]:
				# Unit info and movement choises if any
				if nav.unit.movement > 0:
					# geting the neighbors hexagons with distance equal to movement
					dic = self.current_highlight_region.getRange(nav, nav.unit.movement)
					# Highlight the list of neighbors hexagons
					self.highlightNav(dic.keys())
					self.board_move_list = nav, dic
			self.updateMap()
		else:
			self.killActionWindow()
			self.board_move_list = None
			self.highlightNav([])
			self.updateMap()

	def checkAction(self, src, dest):
		""" Check what action will be used against a destination unit """
		# get the availiable actions
		l = SDATA.getActionList(src.navtype, dest.navtype)
		# calculate the space we need for the given actions
		w = len(l) * (self.iWidth("button") + WC.SPACER) + (3 * WC.SPACER)
		h = self.iHeight("button") + (4 * WC.SPACER)
		x = dest.rect.x - (w / 2) + (dest.rect.w / 2)
		y = dest.rect.y - h - WC.SPACER
		# convert to map cords
		x, y = self.mapToScreenCoords(x, y)
		# check if we cross over the map screen boundry
		if x > WC.SCREEN_WIDTH - w: x = WC.SCREEN_WIDTH - w - 1
		elif x < 0:	x = 1
		if y < self.iHeight("win_top"):	y += 2 * (h + WC.SPACER)
		# create the window but don't draw it
		self.action_window = self.addWindow(SWINDOW.CWindow(x, y, w, h, dest.name, False))
		# add the appropriate widgets
		backimage = SWIDGET.CHightlight(0, 0, w, h, WC.T_COLG_BLUE)
		backimage.visible = True
		backimage.active = False
		self.windows[self.action_window].addWidget(backimage)
		for wi in range(len(l)):
			btn = SWIDGET.CButton(wi * (self.iWidth("button") + WC.SPACER) + 2 * WC.SPACER, 2 * WC.SPACER, str(l[wi]))
			btn.visible = True
			btn.active = True
			self.windows[self.action_window].addWidget(btn)
		
	def mainMapClick(self, region, x, y):
		""" checks for gui map clicks on maip map """
		# first we check regions
		if region != None:
			# hightlight the region
			self.highlightRegion(region)
			return
		else:
			# a click on a non-important map area
			# We make a flag for update the screen
			# and we erase the current highlighted fleet/unit
			self.current_highlight_region = None
			# check the system control buttons
			if self.region_highlight != None:
				self.region_highlight.update(self.image("buffer"))
				self.updateMap()
		return True
			
	def highlightNav(self, xlist, color = pygame.Color(255, 255, 255, 102)):
		"""Highlight the hexes in the list"""
		# remove all dirty rect's from the board
		while self.nav_dirty != []:
			self.image("buffer").blit(self.nav_dirty[-1].image, self.nav_dirty[-1].rect)
			self.nav_dirty.pop()
		# if there is no selection return false
		if xlist == []: return False
		# draw the highlighted list of navs
		mask = pygame.Surface((xlist[0].rect.w, xlist[0].rect.h), pygame.SRCALPHA, 32)
		pygame.draw.polygon(mask, color, self.current_highlight_region.points(1,1), 0)
		for i in xlist:
			# add a dirty rect of this Hex
			r_high = pygame.Surface(mask.get_size(), 32)
			r_high.blit(self.image("buffer"), (0, 0), i.rect)
			self.nav_dirty.append(CDirtyRect(r_high, i.rect))
		for i in xlist:
			# highlight the Hex
			self.image("buffer").blit(mask, (i.rect.x, i.rect.y))
		return True
		
	def highlightRegion(self, region, color = pygame.Color(255, 0, 0, 102)):
		"""Highlight the region"""
		if self.region_highlight != None:
			self.region_highlight.update(self.image("buffer"))
		if self.board == True:
			mask = pygame.Surface((region.rect.w, region.rect.h), pygame.SRCALPHA, 32)
			pygame.draw.polygon(mask, color, self.current_highlight_region.points(1,1), 0)
			r_high = pygame.Surface(mask.get_size(), 32)
		else:
			# fill with colour of player
			mask = self.image(region.mask).copy()
			highlight = pygame.Surface(self.image(region.mask).get_size(), 32)
			# fill with colour of player
			highlight.fill(WC.COL_WHITE) 
			mask.blit(highlight, (0, 0), None, pygame.BLEND_ADD)
			# add a dirty rect of this
			r_high = pygame.Surface(self.image(region.mask).get_size(), 32)
		r_high.blit(self.image("buffer"), (0, 0), region.rect)
		self.region_highlight = CDirtyRect(r_high, region.rect)
		self.image("buffer").blit(mask, (region.rect.x, region.rect.y))
		if self.board == False: self.current_highlight_region = region
		self.updateMap()

	# this is the main game loop. There are 2 varients of it, one which keeps
	# looping forever, and a solo version which runs only once
	def mainLoop(self):
		"""CGFXEngine.mainLoop() - call with nothing"""
		while True:
			event = pygame.event.wait()
			# ok main loop: after setting everything up, just keep calling self.checkInputs()
			self.clock.tick()
			self.checkInputs(event)

	def mainLoopSolo(self):
		"""As mainLoop, but only for 1 event"""
		event = pygame.event.wait()
		self.clock.tick()
		self.checkInputs(event)

	# this is the function that allows you to pan the the map with the 
	# middle mouse button
	def panMap(self):
		"""Allows user to pan map with middle click"""
		xpos, ypos = pygame.mouse.get_rel()
		while True:
			event = pygame.event.poll()
			# check the middle button is still pressed
			a, b, c = pygame.mouse.get_pressed()
			if b != 1:
				# mouse has been de-pressed
				return			
			if event.type == MOUSEMOTION:
				# grab relative grabs
				xpos, ypos = pygame.mouse.get_rel()
				# update the map thus
				self.map_screen.x -= xpos
				self.map_screen.y -= ypos
				# check the scroll areas...
				self.normalizeScrollArea()
				# and then finally draw it!
				self.updateMiniMap()
				self.updateMap()

	def blitCheckbox(self, widget, xpos, ypos):
		"""Renders a checkbox at the given location
		   Very simple, just used to isolate gfx drawing out
		   of the checkbox widget code"""
		# we need to blit the buffer copy first
		self.screen.blit(self.image("buffer"),
						 (xpos + self.map_screen.x,
						  ypos + self.map_screen.y + self.iHeight("win_tl"),
						  widget.rect.w, widget.rect.h))
		self.screen.blit(widget.image, (xpos, ypos, 0, 0))		
		pygame.display.update((xpos, ypos, widget.rect.w, widget.rect.h))
		return True
		
	def blitSlider(self, xpos, ypos, width, height, image):
		"""Renders the slider bar at given position"""
		# just blit image and update
		self.screen.blit(image, (xpos, ypos, 0, 0))
		pygame.display.update((xpos, ypos, width, height))
		return True
		
	def blitScrollarea(self, xpos, ypos, width, height, image):
		"""Renders the scrollarea at given position"""
		# very similar to blitSlider
		self.screen.blit(image, (xpos, ypos, 0, 0))
		pygame.display.update((xpos, ypos, width, height))
		return True
		
	def blitHint(self, xpos, ypos, text, font = WC.FONT_VERA_SM):
		""" Renders a text over the mouse pointer for widget hint """
		# TODO make it work with mouse pointer cords
		if self.current_hint != None:
			self.hint = None
			self.current_hint = None
		new = self.fonts[font].render(text, 1, WC.COL_WHITE)
		rectangle = pygame.Rect(xpos - (new.get_width()/2),
					ypos - self.fonts[font].size(text)[1],
					new.get_width(),
					new.get_height())
		tempsurface = pygame.Surface((rectangle.w, rectangle.h))		
		tempsurface.blit(pygame.display.get_surface(), (0, 0), rectangle)
		# now blit the new image
		self.killHint()
		self.screen.blit(new, (rectangle.x, rectangle.y))
		self.hint = CDirtyRect(tempsurface, rectangle)
		pygame.display.update(rectangle)
	
	def switchMap(self,region):
		""" Changing the main map and updates the gui. If region is None
			then we return to the main map """
		# if None then return to the main map
		if region == None :
			# Enable mini map
			self.minimap_widget.active = True
			self.minimap_widget.visible = True
			# where to center the map. this time on the last highlighted region
			cx = self.region_highlight.rect.x
			cy = self.region_highlight.rect.y
			# copy the map to our buffer 
			# and here we blit any additional map surfaces if needed
			self.images["buffer"] = self.images["map"].copy()
			# removing highlights and changing active buttons		
			self.region_highlight = None
			self.current_highlight_region = None
			# flip the widgets
			for i in self.board_map_widgets:
				# clear all the board map widgets
				i.visible = False
				i.active = False
			for i in self.main_map_widgets:
				# enable all the system map widgets
				i.visible = True
				i.active = True
			# disable board flag and reset dirty hexes
			self.nav_dirty = []
			self.hint = None
			self.board = False
			# enable animation timer
			self.timer = True
		else:
			# Disable mini map
			self.minimap_widget.active = False
			self.minimap_widget.visible = False
			# we get the new map from the regions name and we store it in a temp
			# surface self.images["board"]. This is done once every time we enter a system map
			self.images["board"] = self.images[region.sysmap].copy()
			# draw the grid if debug is on
			if WC.DEBUG_MODE == True:
				self.images["board"].blit(self.hex_grid_draw(region.sysmap,region.colour,region.navs), (0, 0))
			# draw the units and to the board surface and display it
			self.renderBoard(region)
			#########################################################
			# TODO insert offsets to center the board map to screen #
			#########################################################
			# where to center the map. this time in the middle of it
			cx = self.iWidth("buffer")/2
			cy = self.iHeight("buffer")/2
			# flip the widgets
			for i in self.main_map_widgets:
				# clear all the system map widgets
				i.visible = False
				i.active = False
			for i in self.board_map_widgets:
				# enable all the board map widgets except the first
				i.visible = True
				i.active = True
			# enable board flag and reset dirty hexes
			self.nav_dirty = []
			self.hint = None
			self.board = True
			# disable animation timer
			self.timer = False
		# store a rect of the maximum map limits we can scroll to
		self.map_max_x = self.iWidth("buffer") - WC.SCREEN_WIDTH
		self.map_max_y = self.iHeight("buffer") - self.map_rect.h	
		# center the map in the middle or on the last highlighted region and update the GUI
		self.centerMap(cx,cy)
		# Clear the screen from the main Map
		self.screen.fill(WC.COL_BLACK)
		# Updating the screen and all the GUI
		self.updateGUI()
		
	def renderBoard(self, region):
		""" Render the image for the board map with hexes and offcource the units"""
		self.images["buffer"] = self.images["board"].copy()
		self.images["buffer"].blit(self.blitnavs(region), (0,0))
		
	def renderUnitInfoBox(self, unit):
		""" Render the image for the widget that shows the unit info """
		if unit == None:
			self.info_widget.visible = False
			self.info_widget.active = False
		else:
			info = pygame.Surface(self.image("small_map").get_size()).convert_alpha()
			info.fill(WC.BGUI_COL)
			icon = self.image(unit.name)
			pygame.draw.rect(info, WC.COL_BLACK,
							pygame.Rect(0, 0, info.get_width(), info.get_height()), 1)
			info.blit(icon, (info.get_width() - (icon.get_width() + 8), 8))
			# render the region name, as well
			self.info_widget.image = info
			self.info_widget.visible = True
			self.info_widget.active = True

# Map functions

	def blitnavs(self, region):
		""" Function to blit the navpoints into main map image """
		# define alpha channel image surface for bliting
		foo = pygame.Surface(self.images[region.sysmap].get_size(),SRCALPHA)
		# blit images
		for nav in region.navs:
			if nav.unit!= None: nav.unit.drawinHex(foo)
		return foo
		
	def unitAnim(self, src, dest):
		""" set a unit in motion between two hexes """
		# move the unit's data
		if self.current_highlight_region.moveUnit(src, dest) == True:
			self.images["buffer"].blit(self.images["board"].subsurface(src.rect).copy(),src.rect)
			dest.unit.drawinHex(self.images["buffer"])
			return True
		else: return False

	def hex_grid_draw(self, image, color, hexes):
		""" draws an aplha channel hex grid upon the image and returns it.
			image = the image that we blit the grid
			color = hex border color
			hexw,hexh = how many hexes the grid will have """
		# define hex grid surface
		foo = pygame.Surface(self.images[image].get_size(),SRCALPHA)
		foo.blit(self.images[image], (0,0))
		# we going to draw the hexes
		for h in hexes:
			# draw the hexagon at the given position
			pygame.draw.aalines(foo, color, True, self.current_highlight_region.points(h.rect.x,h.rect.y), False)
			# Show the hexagon map location in the center of the tile.
			if WC.DEBUG_MODE==True:
				location = self.fonts[WC.FONT_MSG].render("%d,%d" % (h.cellX,h.cellY), 0, WC.COL_RED)
				lrect=location.get_rect()
				lrect.center = (h.rect.x+(self.current_highlight_region.width/2),h.rect.y+(self.current_highlight_region.height/2))
				foo.blit(location,lrect.topleft)
		return(foo)
		
	def createCursor(self, cursor_color, cursor_width = 0):
		"""	Create the cursor. """
		self.cursor = pygame.Surface((SDATA.data.map.width+4, SDATA.data.map.height+4), pygame.SRCALPHA, 32)
		# draw a hexagon for the cursor. (1,1) x,y offset for antialliasing
		pygame.draw.polygon(self.cursor, cursor_color, SDATA.data.map.points(1,1), cursor_width)
		self.cursorPos = self.cursor.get_rect()

	def setCursor(self, x, y):
		"""	Set the hexagon map cursor.	"""
		mapX,mapY = SDATA.data.map.pixel2HexMap(x,y)
		if WC.DEBUG_MODE == True: print "Hexagon coords: " + str(mapX) + " , " + str(mapY)
		pixelX,pixelY = SDATA.data.map.hexMap2Pixel(mapX, mapY)
		x, y = self.mapToScreenCoords(pixelX, pixelY)
		self.cursorPos.topleft = (x, y)
	
# TODO: Below code should be moved into widgets.py

	def story(self, image, caption, dialog, xpos, ypos, font = WC.FONT_VERA, txtcolor = WC.COL_BLACK):
		"""Routine that displays dialogs on screen
		   Always returns True after doing it's work"""
		# set the sizes
		width = self.iWidth(image)
		height = self.iHeight(image)
		fontwidth, fontheight = self.fonts[font].size("X")
		# Don't be bigger than 90% of main map
		while width < (self.map_area.w * 0.9) and height < (self.map_area.h * 0.9):
			width += int(width * 0.1)
			height += int(height * 0.1)
		# how many letters we can blit
		letters = int(width // fontwidth)
		# resize the pic
		bckimg = pygame.transform.scale(self.image(image), (width, height))
		# set sizes without borders
		w = width - (2 * WC.WINSZ_SIDE)
		h = height - WC.WINSZ_TOP - WC.WINSZ_BOT
		# build the window
		index = self.addWindow(SWINDOW.CWindow(xpos, ypos, w, h, caption, False))
		# add the backround image
		self.windows[index].image.blit(bckimg, (0, 0))
		# add 2 image widgets to use 1st will be a picture
		imgx = width / 2  - self.iWidth(dialog[0]["name"])/2
		imgy = WC.SPACER
		imgw = self.iWidth(dialog[0]["name"])
		imgh = self.iHeight(dialog[0]["name"])
		img = SWIDGET.CImage(imgx, imgy, imgw, imgh, None)
		self.windows[index].addWidget(img)
		# creating the background of the 1st widget
		bckwga = bckimg.subsurface(img.rect)
		# 2nd widget will be the text
		lbly = img.rect.y + WC.SPACER + self.iHeight(dialog[0]["name"])
		lbl = SWIDGET.CImage(width*0.1, lbly, width * 0.8, height - lbly - 2 * WC.SPACER, None)
		self.windows[index].addWidget(lbl)
		# create the background of the 1st widget
		bckwgb = bckimg.subsurface(lbl.rect)
		bckwgbw, bckwgbh = bckwgb.get_size()
		# Make window modal
		self.windows[index].modal=True
		# set keyboard functions
		self.keyboard.addKey(K_RETURN, SEVENT.msgboxOK)
		self.keyboard.addKey(K_SPACE, SEVENT.msgboxOK)
		self.keyboard.addKey(K_ESCAPE, SEVENT.msgboxQuit)
		self.keyboard.setModalKeys(3)
		# draw window
		self.addDirtyRect(self.windows[index].drawWindow(),
			self.windows[index].rect)
		offset = self.windows[index].rect
		# loop all the dialog npcs
		for i in range(len(dialog)):
			# getting the picture
			self.windows[index].items[0].image.blit(bckwga, (0, 0))
			self.windows[index].items[0].image.blit(self.image(dialog[i]["name"]), (0, 0))
			txtheight = -3
			pygame.event.clear()
			for j in dialog[i]["lines"]:
				# getting the dialoge text and blit for each line
				txtheight += fontheight + 3
				txt = 0
				# keep track of time
				pygame.time.set_timer(WC.EVENT_TIME, WC.TEXT_DELAY)
				while len(j)<letters:
					# blit the text each letter at a time
					tempsurface = self.fonts[font].render(j[0:txt], 1, txtcolor)
					self.windows[index].items[1].image.blit(bckwgb.subsurface(0, txtheight, bckwgbw, bckwgbh - txtheight),(0,txtheight))
					self.windows[index].items[1].image.blit(tempsurface, (0, txtheight))
					self.updateOverlayWindow(index)
					pygame.display.update(offset)
					txt += 1
					# check if we have end of line
					if txt>len(j): break
					# keep looping until we get a positive result
					self.callback_temp = WC.BUTTON_FAIL
					while self.callback_temp == WC.BUTTON_FAIL:
						if self.tick==False:
							self.tick=True
							break
						self.mainLoopSolo()
					if self.callback_temp == WC.BUTTON_OK:
						tempsurface = self.fonts[font].render(j, 1, txtcolor)
						self.windows[index].items[1].image.blit(bckwgb.subsurface(0,txtheight,bckwgbw,bckwgbh-txtheight),(0,txtheight))
						self.windows[index].items[1].image.blit(tempsurface,(0,txtheight))
						self.updateOverlayWindow(index)
						pygame.display.update(offset)
						break
					elif self.callback_temp == WC.BUTTON_QUIT:
						pygame.time.set_timer(WC.EVENT_DC_END, 0)
						# redraw the screen
						self.deleteTopDirty()
						# reset the keyboard
						self.keyboard.removeModalKeys()
						# remove the window
						self.windows.pop()
						return True
				pygame.time.set_timer(WC.EVENT_DC_END, 0)
			# check for end of text
			self.callback_temp = WC.BUTTON_FAIL
			while self.callback_temp == WC.BUTTON_FAIL:
				if self.tick==False:
					pygame.time.wait(WC.END_DIALOG)
					self.tick=True
					break
				self.mainLoopSolo()
		# redraw the screen
		self.deleteTopDirty()
		# reset the keyboard
		self.keyboard.removeModalKeys()
		# remove the window
		self.windows.pop()
		return True
	
# this module acts as the GUI singleton
gui = CGFXEngine()

# TOP RIGHT arc from a circle
#pygame.draw.arc(screen, BLACK,[210, 75, 150, 125], 0, pi/2, 2)
# TOP LEFT arc from a circle
#pygame.draw.arc(screen, GREEN,[210, 75, 150, 125], pi/2, pi, 2)
# BOTTOM LEFT arc from a circle
#pygame.draw.arc(screen, BLUE, [210, 75, 150, 125], pi,3*pi/2, 2)
# BOTTOM RIGHT arc from a circle
#pygame.draw.arc(screen, RED,  [210, 75, 150, 125], 3*pi/2, 2*pi, 2)
