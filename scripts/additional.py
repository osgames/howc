#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 cstamoultas <cstamoultas@IT-DSK2>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import networkx as nx

def dist(a, b):
	(x1, y1) = a
	(x2, y2) = b
	return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5


def main():
	G=nx.grid_graph(dim=[3,3])
	G.add_edge((0,0),(1,1), weight=0.3)
	G.add_edge((2,2),(1,1), weight=0.3)
	G.add_edge((2,0),(1,1), weight=0.3)
	G.add_edge((0,2),(1,1), weight=0.3)
	print(nx.astar_path(G,(0,0),(2,2),dist))

if __name__ == '__main__':
	main()

class CGame(object):
	def addGUIelements(self):
		""" Here we configure how our GUI will look like
			and we return a window that contains that widgets. """
	
		######## CGuiImage widget example ################
		# This is a round circle button like widget with text inside it
		##################################################
		enter = SWIDGET.buildGuiImage("infhex", "Enter System", True)
		enter.rect.x = WC.SCREEN_WIDTH - WC.SPACER - SGFX.gui.iWidth("infhex")
		enter.rect.y = SGFX.gui.iWidth("infhex") - (2*WC.SPACER)
		enter.describe = "Exit system map"
		enter.visible = True
		enter.active = True
		enter.callbacks.mouse_lclk = SEVENT.enter
		enter.callbacks.mouse_over = SEVENT.hint
		bwindow.addWidget(enter)
		SGFX.gui.system_widgets.append(enter)
		#-------------------------------------------------
		
		######## CImage widget example ###################
		# This is an image widget that acts like an info box
		# this goes in the bottom left corner of the screen
		##################################################
		image = pygame.Surface(SGFX.gui.image("small_map").get_size()).convert_alpha()
		image.fill(WC.BGUI_COL)
		info = SWIDGET.buildUniqueImage(image)
		info.rect.x = WC.SPACER
		info.rect.y = y
		info.describe = "info-box"
		info.visible = False
		info.callbacks.mouse_lclk = SEVENT.centerMap
		bwindow.addWidget(info)
		SGFX.gui.info_widget = info
		#-------------------------------------------------
		
		######## CImage system control widget ############
		# This is an image widget that acts like a button
		##################################################
		wi = SWIDGET.CGuiImage(x, y - SGFX.gui.iHeight("tab"),
			SGFX.gui.iWidth("tab"), SGFX.gui.iHeight("tab"),"tab","System")
		wi.describe = "System"
		wi.visible = True
		wi.active = True
		wi.callbacks.mouse_lclk = SEVENT.enter
		wi.callbacks.mouse_over = SEVENT.hint
		SGFX.gui.main_map_widgets.append(wi)
		bwindow.addWidget(wi)
		#-------------------------------------------------
		
		########   CHightlight board map area    #########
		# This is a highlighted area for the board widgets
		##################################################
		high = SWIDGET.CHightlight((WC.SCREEN_WIDTH - 100)/2,
								WC.SCREEN_HEIGHT - SGFX.gui.map_rect.h,
								WC.ICON_SIZE * 5 + 4 * WC.SPACER,
								WC.ICON_SIZE + 2 * WC.SPACER, WC.T_SEP_DARK)
		high.visible = True
		high.active = True
		SGFX.gui.board_map_widgets.append(high)
		bwindow.addWidget(high)
		#-------------------------------------------------

		######## CImage board map control widget #########
		# This is an image widget that acts like a button
		##################################################
		wi = SWIDGET.CImage(high.rect.x + WC.SPACER, high.rect.y + WC.SPACER,
			SGFX.gui.iWidth("exit"), SGFX.gui.iHeight("exit"),"exit")
		wi.describe = "System"
		wi.visible = False
		wi.active = False
		wi.callbacks.mouse_lclk = SEVENT.exits
		wi.callbacks.mouse_over = SEVENT.hint
		SGFX.gui.board_map_widgets.append(wi)
		bwindow.addWidget(wi)
		#-------------------------------------------------
	
				
		######## CGuiObj widget ######################
		# Area for fleet/unit selection and managment
		##################################################
		xpos = int((WC.SCREEN_WIDTH - (WC.UNIT_WIDTH * 9)) / 2)
		ypos = SGFX.gui.map_rect.height - ((3 * WC.UNIT_HEIGHT) + (2 * WC.SPACER))
		fl = SWIDGET.CGuiObj(-1, ypos, WC.T_SEP_DARK,[None,[None]],[None,[None]])
		fl.visible = False
		fl.active = False
		SGFX.gui.obj_widget = fl
		bwindow.addWidget(fl)
		#-------------------------------------------------


class CGuiObj(CWidget):
	""" Object slider class that presents and manages details of game data """	
	def __init__(self, x, y, bgcolor, objlist, objaction, disp = True):
		# objlist is a list of objects . same goes for objaction
		# for every row we have a list of objects to display and
		# a list of action for every object stored in objaction
		self.frame = "unit_background"
		CWidget.__init__(self, pygame.Rect(x, y, 0, 0), WC.WT_SEP,
			pygame.Surface((0,0)), "CGuiObjSlider")
		# initialise object properties
		self.cols = 0
		self.bgcolor = bgcolor
		self.data = objlist
		self.objlist = []
		self.objaction = objaction
		# the rects for the objects
		self.rects = []
		# define border spacing of the hightlighted backround
		self.border = pygame.Rect(WC.SPACER, WC.SPACER, 2*WC.SPACER, 2*WC.SPACER)
		self.disp_border = disp
		# create indexes for displaying and highlighting
		self.index = 0
		self.high = -1
		self.arrows = False
		# automatically add it's own click callback
		self.callbacks.mouse_lclk = self.clicked
		
	def reset(self):
		""" resets the widget """
		self.image = pygame.Surface((0,0))
		self.cols = 0
		self.data = [None,[None]]
		self.objlist = []
		self.objaction = [None,[None]]
		self.rects = []
		self.index = 0
		self.high = -1
		self.arrows = False
		
	def objects(self):
		objlist = []
		j = []
		if self.data[0] == None: objlist.append("taskforse")
		else: objlist.append(self.data[0].ship)
		for i in self.data[1]: j.append(i.name)
		objlist.append(j)
		return objlist
		
	def update(self):
		""" Routine that calculates the width, height and plus the border spacing
			Furthermore it displays the objects with directional arrows if needed """
		# check if we have any objects to display
		if self.data == [None,[None]]: return False
		# populating objects
		self.objlist = self.objects()
		# making the main object image
		self.mainimage = SGFX.gui.image(self.objlist[0]).copy()
		# if we don't have any objects to render exit
		if self.objlist[1] == []:
			print "HOWC error: no object to render"
			sys.exit(0)
		# check for max objects dislayed
		if len(self.objlist[1]) > WC.MAX_CONTROLS:
			self.cols = WC.MAX_CONTROLS
			self.arrows = True
		else:
			self.cols = len(self.objlist[1])
			self.arrows = False
		# calculating the width plus the border spacing
		self.rect.w = (self.cols * (SGFX.gui.image(self.frame).get_width() + WC.SPACER)
			+ self.border.w + self.mainimage.get_width() + WC.SPACER)
		# calculating the height plus the border spacing
		if (self.mainimage.get_height() + self.border.h) >= (SGFX.gui.images[self.frame].get_height() + self.border.h):
			self.rect.h = self.mainimage.get_height() + self.border.h
		else:
			self.rect.h = SGFX.gui.images[self.frame].get_height() + self.border.h
		# if x or y is -1 then we center vertical or horizontal or both
		if self.rect.x == -1: self.rect.x = (WC.SCREEN_WIDTH - self.rect.w)/2
		if self.rect.y == -1: self.rect.y = (WC.SCREEN_HEIGHT - self.rect.h)/2
		# next we add the rects with the objects and first the main object
		self.rects.append(pygame.Rect(self.border.x, (self.rect.h-self.mainimage.get_height())/2,
			self.mainimage.get_width(), self.mainimage.get_height()))
		# now the rest of the rects	where rects[0] the main object
		for j in range(self.cols):
			self.rects.append(pygame.Rect(
				self.border.x + WC.SPACER + self.rects[0].w + j*(WC.SPACER + SGFX.gui.images[self.frame].get_width()),
				(self.rect.h - SGFX.gui.images[self.frame].get_height())/2,
				SGFX.gui.images[self.frame].get_width(),
				SGFX.gui.images[self.frame].get_height()))
		# for every row we display the objects
		self.render()
		return True
				
	def render(self):
		""" Blits the images to the screen """
		# make the widget image starting with the highlighted area
		self.image = hlbox(self.rect.w, self.rect.h, self.bgcolor)
		# render the main object
		self.image.blit(self.mainimage, self.rects[0])
		if self.disp_border == True:
			pygame.draw.line(self.image, WC.SEP_LIGHT, (self.border.x + self.mainimage.get_width() + 2, 4),
				(self.border.x + self.mainimage.get_width() + 2, self.rect.h - 4))
		# render arrows if it is needed and making a pointer
		arrow = 0
		if self.arrows == True:
			tmpimage = SGFX.gui.images[self.frame].copy()
			tmpimage.blit(SGFX.gui.images["l_arrow"], (0, 0))
			self.image.blit(tmpimage, (self.rects[1].x, self.rects[1].y))
			tmpimage = SGFX.gui.images[self.frame].copy()
			tmpimage.blit(SGFX.gui.images["r_arrow"], (0, 0))
			self.image.blit(tmpimage, (self.rects[-1].x, self.rects[-1].y))
			arrow = 1
		for j in range(self.cols- 2*arrow):	
			# now we calculate the displaying images from self.index
			# which is the first image that we display from the list
			tmpimage = SGFX.gui.images[self.frame].copy()
			if self.high == j + self.index:
				highlight = pygame.Surface(SGFX.gui.images[self.frame].get_size(), 32)
				highlight.fill(WC.T_SEP_DARK)
				tmpimage.blit(highlight, (0, 0), None, pygame.BLEND_ADD)
			tmpimage.blit(SGFX.gui.images[self.objlist[1][j + self.index]], (0, 0))
			self.image.blit(tmpimage, (self.rects[j + 1 + arrow].x, self.rects[j + 1 + arrow].y))

	def clicked(self, handle, x, y):
		""" Routine to find the position of the objected pressed """
		# check which rect is clicked
		collide = False
		for r in range(len(self.rects)):
			if self.rects[r].collidepoint(x, y):
				collide = True
				# check which object is clicked
				if r == 0:
					self.objaction[0](self, x , y)
				elif r == 1 and self.arrows == True:
					if self.index > 0:
						self.index -= 1
				elif r == self.cols and self.arrows == True:
					if self.index < len(self.objlist[1]) - self.cols + 2:
							self.index += 1
				else:
					# try to execute the action and highlight the object
					if ((self.arrows == True) and (1<r<self.cols)) or self.arrows == False:
						# check if we have arrows for highlights
						if self.arrows == True: self.high = r + self.index - 2
						else: self.high = r + self.index - 1
						try:
							self.objaction[1][r-1](self, x , y)
						except IndexError:
							if WC.DEBUG_MODE == True: print 'Object clicked: Index list out of range'
		# Nothing to highlight
		if collide == False: self.high = -1
		self.render()
		SGFX.gui.updateGUI()
		
class CGFXEngine(object):		
	def renderRegionInfoBox(self, region):
		"""Render the image for the widget that shows the region info"""
		info = pygame.Surface(self.image("small_map").get_size()).convert_alpha()
		info.fill(WC.BGUI_COL)
		rect_icon = (SDATA.getRegion(region).rect.x - ((WC.REGION_ICON_SIZE - self.image("mask").get_width()) / 2),
					SDATA.getRegion(region).rect.y - ((WC.REGION_ICON_SIZE - self.image("mask").get_height()) / 2),
					WC.REGION_ICON_SIZE, WC.REGION_ICON_SIZE)
		icon = self.images["map"].subsurface(rect_icon)
		pygame.draw.rect(info, WC.COL_BLACK,
						 pygame.Rect(0, 0, info.get_width(), info.get_height()), 1)
		info.blit(icon, (info.get_width() - (icon.get_width() + 8), 8))
		# render the region name, as well
		self.info_widget.image = info
		self.info_widget.visible = True
		self.info_widget.active = True
			
#########################################
# we scale the new map to mini man		#
# if we need to use the mini map		#
# on the board screen and recalculate	#
# the ratio for mini map				#
#########################################
#pygame.transform.scale(self.images[region.sysmap].convert_alpha(),
#						(self.iWidth("small_map"),
#						 self.iHeight("small_map")),
#						 self.images["small_map"])
#self.width_ratio = float(self.iWidth("buffer")) / float(self.iWidth("small_map") - 2)
#self.height_ratio = float(self.iHeight("buffer")) / float(self.iHeight("small_map") - 2)
#self.blit_rect.w = int(float(self.map_rect.w) / self.width_ratio)
#self.blit_rect.h = int(float(self.map_rect.h) / self.height_ratio)
