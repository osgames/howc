#!/usr/bin/python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# we need some pygame variables:
import pygame.locals as PYGAME
from math import sin, radians

# this file contains the global defines for HOWC
# as you can see, there are quite a few. However, most should be left
# as they are, unless you really know what you are doing. Probably the
# most useful when debugging is FULLSCREEN
#########################################
# General defines
#########################################

VERSION				= "v0.2.115"
AUTHOR				= "Chris Stamoultas"
EMAIL				= "nemecis@users.sourceforge.net"
WEBSITE				= "http://sourceforge.net/projects/howc"
STARTED				= "Aug 2012"

#########################################
# Init defines
#########################################

# standar scenario, we put the folder's name
SCENARIO		= "main"

# currently adds debug messages to the terminal and creates logs
DEBUG_MODE		=	True

# Screen size ( will be modifided throuht config file )
# This is the debuging screen ress.
SCREEN_WIDTH 		= 1024
SCREEN_HEIGHT 		= 768
SMALL_MAP_W			= 200
SMALL_MAP_H			= 156
FULLSCREEN			= False
INTRO				= False
LOADSCREEN			= False

#########################################
# Display, Fonts and Sizes defines
#########################################

# set this next one to true if you want a rh mouse click to exit
RMOUSE_END			= False

# menu icons are always squares
ICON_SIZE			= 24
# checkbox size goes here (always a square)
CHKBOX_SIZE			= 13
# size of unit gfx
UNIT_WIDTH			= 45
UNIT_HEIGHT			= 41
# box size for region icons
REGION_ICON_SIZE	= 85
REGION_SIZE			= 40

KSCROLL_SPD			= 80

# maximum number of units
MAX_STACKING		= 4
MAX_UNITS			= 7

# sizes of various gradiant bars used in ItemList widget
GRADBAR_SIZES		= [64, 96, 128]
GRADBAR_NAMES		= ["gradbar64", "gradbar96", "gradbar128"]
GRADBAR_WIDTH		= 128

# fonts used by the game
FONT_VERA			= 0
FONT_VERA_SM		= 1
FONT_VERA_LG		= 2
FONT_VERA_BOLD		= 3
FONT_VERA_ITALIC	= 4
# and their sizes
FONT_SMALL			= 12
FONT_STD			= 14
FONT_LARGE			= 16
# messagebox font
FONT_MSG			= FONT_VERA

#########################################
# Timers and interupt events
#########################################

# milliseconds between unit flash
# (all animation times are in milliseconds)
ANIM_TIME			= 40
# number of milliseconds between clicks in a double-click (400 is the Gnome standard)
DCLICK_SPEED		= 400
# Number of milliseconds between time ticks for text blit
TEXT_DELAY			= 60
# Dialog delays in microseconds
END_DIALOG			= 2000

# event used by unit flash animation
EVENT_ANIM			= PYGAME.USEREVENT
# event used by music
EVENT_SONGEND		= PYGAME.USEREVENT + 1
# stop looking for a double-click when we get this event
EVENT_DC_END		= PYGAME.USEREVENT + 2
# time event
EVENT_TIME			= PYGAME.USEREVENT + 3

#########################################
# standard buttons that the messagebox
# function uses
#########################################

# index numbers of windows that are always present
WIN_MENU			= 1

# mouse events as seen by the gui
MOUSE_NONE			= 0
MOUSE_OVER			= 1
MOUSE_LCLK			= 2
MOUSE_RCLK			= 3
MOUSE_DCLICK		= 4
MOUSE_LDOWN			= 5
MOUSE_RDOWN			= 6

# standard buttons that the messagebox function uses
BUTTON_FAIL			= 0
BUTTON_OK			= 1
BUTTON_CANCEL		= 2
BUTTON_YES			= 4
BUTTON_NO			= 8
BUTTON_QUIT			= 16
BUTTON_IGNORE		= 32

# standard widget types
WT_ROOT				= 0
WT_MAP				= WT_ROOT
WT_BUTTON			= 1
WT_LABEL			= 2
WT_IMAGE			= 3
WT_SEP				= 4
WT_CHECK			= 5
WT_MENU				= 6
WT_SLIDER			= 7
WT_SCROLLAREA		= 8
WT_ITEMLIST			= 9
WT_OPTMENU			= 10
WT_TEXT				= 11

# text layout types
LEFT_JUSTIFY		= 0
RIGHT_JUSTIFY		= 1
CENTER_HORIZ		= 2
EVEN				= 3

#########################################
# Menu defines
#########################################

# offsets for when we draw a pop-up menu to screen
MENU_X_OFFSET		= 2
MENU_Y_OFFSET		= 23
# amount of pixels left empty on lhs of any menu
MNU_LSPACE			= 4
# amount of pixels padded out above and below a menu entry
# (distance between menu texts is twice this number)
MNU_HSPACE			= 4
# pixels on rhs left blank on menu
MNU_RSPACE			= 8
# minimum gap between menu text and key text in menu dropdown
MNU_KEY_GAP			= 12
# any other random spacing we need
SPACER				= 8
HALFSPCR			= 4
QTRSPCR				= 2
# offsets when overlaying a move value over a unit
MV_OVER_X			= 6
MV_OVER_Y			= 10
# minimum height of scroll area handle
SCAREA_MINH			= 32

# sizes of the window borders
WINSZ_SIDE			= 6
WINSZ_TOP			= 24
WINSZ_BOT			= 6

# alpha is from 0 to 255, where 0 is transparent
MENU_ALPHA			= 64
# colour of the highlight
MENU_HLCOL			= (170, 83, 83)

#########################################
# Hexes and Map defines
#########################################

# the images to load
# just list the folders inside gfx: the game will pull all of the png files
GRAPHICS_F			= ["gui", "icons", "npc"]

# Set the secondary board maps and their size
GFX_MAP				= ["cloud","comet","galaxy","helix-nebula","sombrero","whirlpool"]
GFX_SIZE			= (1280,800)

# Hexes variables. 
HEX_SIDE			= 40

#########################################
# Gameplay Costants
#########################################

# System Widgets
SYS_WIDGETS = 3
SYS_SIZE = 50

# maximum columns on CGuiObjGrid
MAX_CONTROLS		= 6

#########################################
# Unit Actions
#########################################
ATTACK		= "Attack"
BOARD		= "Board"
LAND		= "Land"
BOMBARD		= "Bombard"
JUMP		= "Jump"
MINE		= "Mining"
SCRAMBLE	= "Scramble Ships"

#########################################
# Gameplay Costants
#########################################

# define all the colours we use as well
BGUI_TXT			= (246, 246, 246)
BGUI_COL			= (238, 238, 230)
BGUI_HIGH			= (227, 219, 213)
MENU_COL			= (246, 246, 246)
MENU_BDR_COL		= (220, 220, 220)
MENU_CNR_COL		= (194, 194, 194)
MENU_TXT_COL		= (0, 0, 0)
COL_BLACK			= (0, 0, 0)
COL_WHITE			= (255, 255, 255)
COL_RED				= (255, 0, 0)
COL_GREEN			= (0, 255, 0)
COL_BLUE			= (0, 0, 255)
COL_YELLOW			= (255, 255, 0)
COLG_BLUE			= (79, 164, 232)
COLG_RED			= (171, 84, 84)
COLG_RHIGH			= (254, 120, 120)
COL_BUTTON			= (0, 0, 0)
COL_WINTITLE		= (0, 0, 0)
SLIDER_LIGHT		= (116, 133, 216)
SLIDER_MEDIUM		= (98, 113, 183)
SLIDER_DARK			= (81, 93, 151)
SLIDER_BDARK		= (70, 91, 110)
SLIDER_BLIGHT		= (170, 156, 143)
SLIDER_BMED1		= (192, 181, 169)
SLIDER_BMED2		= (209, 200, 191)
SCROLL_BORDER		= (170, 156, 143)
SCROLL_MIDDLE		= (209, 200, 191)
SEP_DARK			= (154, 154, 154)
SEP_LIGHT			= (248, 252, 248)
OPTM_BDARK			= (190, 190, 180)
PLAYER_COL			= (187, 37, 37)

# following trasparent colours

T_SEP_DARK			= (154, 154, 154, 102)
T_COLG_BLUE			= (79, 164, 232, 102)
T_COL_WHITE			= (255, 255, 255, 102)

#########################################
# Audio Costants
#########################################

# some initial values for the sound system
INIT_VOLUME			= 40
MUSIC_BUFFER		= 8192
MUSIC_ON			= False
SFX_ON				= True

# these are the standard callbacks, they should never be called
# they are here to prevent an exception should an unregistered
# event ever be called
def mouse_over_std(handle, x, y):
	print "HOWC Error: mouse_over_std called"
	return False

def mouse_ldown_std(handle, x, y):
	print "HOWC Error: mouse_ldown_std called"
	return False

def mouse_rdown_std(handle,x,y):
	print "HOWC Error: mouse_rdown_std called"
	return False

def mouse_dclick_std(handle, x, y):
	print "HOWC Error: mouse_dclick_std called"
	return False

def mouse_lclk_std(handle, x, y):
	print "HOWC Error: mouse_lclk_std called"
	return False

def mouse_rclk_std(handle, x, y):
	print "HOWC Error: mouse_rclk_std called"
	return False
	
def null_routine(handle, x, y):
	print "HOWC Error: Null routine called"
	return False
