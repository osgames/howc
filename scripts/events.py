#!/usr/bin/python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# get modules
import sys,pygame, yaml, os
from pygame.locals import *

import data as SDATA
import defines as WC
import window as SWINDOW
import widgets as SWIDGET
import gui as SGFX
import sound as SSFX
import menu as SMENU
import ybuild as SYAML

# found here are all the functions triggered by the various mouse events
# they must all have the structure
# def function_name(handle,xpos,ypos)
# xpos and ypos are the offset into whatever
# was clicked, and handle being a pointer to the widget that was clicked

# Important note: keyboard events assume a function like all the rest, i.e. with
# 3 parameters. However, the actual params that get sent on a keypress are
# (0,-1,-1). So make sure that the function doesn't depend on x and y, and
# *DOESN'T* use the handle in a keyboard callback. You can always do code like
# if xpos == -1: to check if it was a keyboard event

#########################################
# General GUI events
#########################################

def changeState(handle, xpos, ypos):
	""" Routine that changes the visible and active state
		of a handle/widget object """
	if handle.visible == True:
		handle.visible = False
	elif handle.visible == False:
		handle.visible = True
	else:
		return False
	SGFX.gui.updateGUI()
	return True
		
def centerMap(handle, xpos, ypos):
	"""Routine centres the map on the city of Rome"""
	# centre map on region
	SGFX.gui.centerMap(SGFX.gui.region_highlight.rect.x,SGFX.gui.region_highlight.rect.y)
	SGFX.gui.updateMiniMap()
	SGFX.gui.updateMap()
	return True

# left click on mini map gives us this
def miniMapClick(handle, xpos, ypos):
	"""Called when user clicks on mini-map
	   Resets map display area and update the screen"""
	# make the click point to the centre:
	# convert to map co-ords
	xpos = xpos * SGFX.gui.width_ratio
	ypos = ypos * SGFX.gui.height_ratio
	# correct to centre of screen
	xpos -= SGFX.gui.map_rect.h / 2
	ypos -= SGFX.gui.map_rect.w / 2
	SGFX.gui.map_screen.x = xpos
	SGFX.gui.map_screen.y = ypos
	# correct if out of range
	SGFX.gui.normalizeScrollArea()
	# update the screen	
	SGFX.gui.updateMiniMap()
	SGFX.gui.updateMap()
	return True

def miniMapDrag(handle, xpos, ypos):
	"""Called when left click dragging over mini-map.
	   Catches all calls until the left mouse button
	   is released again"""
	# if the widget is not visible don't drag
	if handle.visible == False: return True
	miniMapClick(handle, xpos, ypos)
	while(True):
		event = pygame.event.poll()
		if event.type == MOUSEBUTTONUP and event.button == 1:
			# time to exit
			return True
		elif event.type == MOUSEMOTION:
			xpos += event.rel[0]
			ypos += event.rel[1]
			miniMapClick(handle, xpos, ypos)

def killCurrentWindow(handle, xpos, ypos):
	"""Removes current window from window list"""
	# kill the current window
	# let me explain the +1 here: the main loop is a loop that counts the 
	# windows - this where SGFX.gui.win_index comes from. However, since we don't
	# know how the loop is going to go, we increment the index pointer as soon
	# as we have a copy of the window to work with. This means our index is 1
	# out. Since the routine counts down and not up, we are 1 less than we
	# should be, hence the +1 :-)
	SGFX.gui.killIndexedWindow(SGFX.gui.win_index + 1)
	return True
	
def killModalWindow(handle, xpos, ypos):
	"""As killCurrentWindow, but this also removes any modal
	   keypresses we might have left behind. It also destroys the
	   dirty rect that you must have left behind"""
	SGFX.gui.keyboard.removeModalKeys()
	SGFX.gui.deleteTopDirty()
	SGFX.gui.killTopWindow()
	return True


def hint(handle, xpos, ypos):
	""" Displays the hint for the widget """
	xpos += handle.rect.x
	ypos += handle.rect.y
	SGFX.gui.blitHint(xpos, ypos, handle.describe)
	return True

def fleetClick(handle, xpos, ypos):
	""" Change the fleet widgets when clicked """
	pos = int(handle.describe[-1:])
	SGFX.gui.highLightFleetImage(SGFX.gui.current_highlight_region.name, pos)
	SGFX.gui.updateGUI()
	
def unitClick(handle, xpos, ypos):
	""" Change the unit widgets when clicked """
	pos = int(handle.describe[-1:])
	SGFX.gui.highLightUnitImage(SGFX.gui.current_highlight_region.name, pos)
	SGFX.gui.updateGUI()

#########################################
# here come the defines for the menu system, but let's start with a general
# one to say that that part still needs to be coded
#########################################

def notYetCoded(handle, xpos, ypos):
	"""Routine used as a placeholder for various pieces of
	   code until they've actually been done"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop],
		"Sorry, this feature has yet to be coded", "NYC"))

def menuNew(handle, xpos, ypos):
	"""Temp routine, just displays a messagebox for now"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop],
		"Sorry, can't start a new game yet :-(", "New Game"))

def menuLoad(handle, xpos, ypos):
	"""Temp routine, just displays a messagebox for now"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop],
		"Sorry, it is not possible to load games yet.","Load Game"))

def menuSave(handle, xpos, ypos):
	"""Temp routine, just displays a messagebox for now"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop],
		"Sorry, save game has yet to be coded (more code to be done...)",
		"Save Game"))

def menuHelpAbout(handle, xpos, ypos):
	"""Simple messagebox with game info. Returns True"""
	message = "Heroes of Wing Commander " + WC.VERSION + "\n\n"
	message += "Code and Design by " + WC.AUTHOR + "\n\n"
	message += "Thanks Pygame for the library\n"
	message += "Thanks PyYaml and Networkx too\n"
	message += " and Gnome for various GUI graphics."
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], message, "About HOWC"))

def menuHelpHelp(handle, xpos, ypos):
	"""Gateway into help system. Currently a messagebox.
	   Always returns True"""
	message = "Hopefully, as the gui progresses, this area should be a fully "
	message += "functional help database."
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], message, "HOWC Help"))

#########################################
# Sound and music events
#########################################
def musicCheckbox(handle, xpos, ypos):
	"""Starts / stops music"""
	if handle.status:
		# start music
		SSFX.sound.startMusic()
	else:
		# stop music
		SSFX.sound.stopMusic()
	return True
	
def setVolume(handle, xpos, ypos):
	"""Sets volume according to slider value"""
	# get current slider value
	volume = handle.getSliderValue()
	# set new volume
	SSFX.sound.setVolume(volume)
	return True

#########################################
# General button events
#########################################

def keyShowKeys(handle, xpos, ypos):
	"""Displays list of 'standard' keys used in the game"""
	# just a very simple messagebox
	message = "HOWC Keys:\n\n"
	message += "f - Finish unit turn\n"
	message += "k - Show this keylist\n"
	message += "n - highlight next unit\n"
	message += "r - Centre map on Sol System\n"
	message += "F1 - Help\n"
	message += "CTRL+Q - Exit the game\n\n"
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], message, "HOWC Help"))
	return True

# keyboard callbacks to open up the menu
def keyMenuFile(handle, xpos, ypos):
	"""Opens up the menu file"""
	menu = SGFX.gui.windows[WC.WIN_MENU].items[0]
	menu.callbacks.mouse_lclk(menu, menu.offsets[0].x, menu.offsets[0].y)
	return True

def keyMenuEmpire(handle, xpos, ypos):
	"""Opens up the empire file"""
	menu = SGFX.gui.windows[WC.WIN_MENU].items[0]
	menu.callbacks.mouse_lclk(menu, menu.offsets[1].x, menu.offsets[0].y)
	return True

def keyMenuHelp(handle, xpos, ypos):
	"""Opens up the help file"""
	menu = SGFX.gui.windows[WC.WIN_MENU].items[0]
	menu.callbacks.mouse_lclk(menu, menu.offsets[2].x, menu.offsets[0].y)
	return True

def keyMenuDebug(handle, xpos, ypos):
	"""Opens up the dubug menu, if it's there"""
	menu = SGFX.gui.windows[WC.WIN_MENU].items[0]
	menu.callbacks.mouse_lclk(menu, menu.offsets[3].x, menu.offsets[0].y)
	return True

def keyMenuEscape(handle, xpos, ypos):
	"""If escape key is pressed, reset the menu"""
	menu = SGFX.gui.windows[WC.WIN_MENU].items[0]
	# obviously co-ords -1, -1 are never in the menu
	menu.callbacks.mouse_lclk(menu, -1, -1)
	return True

def keyScrollUp(handle, xpos, ypos):
	"""Handles cursor key up event to scroll map"""
	SGFX.gui.map_screen.y -= WC.KSCROLL_SPD
	# check the scroll areas
	SGFX.gui.normalizeScrollArea()
	# and update the screen
	SGFX.gui.updateMiniMap()
	SGFX.gui.updateMap()
	return True

def keyScrollDown(handle, xpos, ypos):
	"""Handles cursor key down event to scroll map"""
	SGFX.gui.map_screen.y += WC.KSCROLL_SPD
	# check the scroll areas
	SGFX.gui.normalizeScrollArea()
	# and update the screen
	SGFX.gui.updateMiniMap()
	SGFX.gui.updateMap()
	return True
	
def keyScrollRight(handle, xpos, ypos):
	"""Handles cursor key right event to scroll map"""
	SGFX.gui.map_screen.x += WC.KSCROLL_SPD
	# check the scroll areas
	SGFX.gui.normalizeScrollArea()
	# and update the screen
	SGFX.gui.updateMiniMap()
	SGFX.gui.updateMap()
	return True
	
def keyScrollLeft(handle, xpos, ypos):
	"""Handles cursor key left event to scroll map"""
	SGFX.gui.map_screen.x -= WC.KSCROLL_SPD
	# check the scroll areas
	SGFX.gui.normalizeScrollArea()
	# and update the screen
	SGFX.gui.updateMiniMap()
	SGFX.gui.updateMap()
	return True

#########################################
# Start up and test events
#########################################

def displayPygameInfo(handle, xpos, ypos):
	"""Simple messagebox to tell user about Pygame"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop],
		"For details on Pygame, vist:\n\nwww.pygame.org\n\nMany thanks to Pete Shinners", "PyGame"))
	return True
	
def windowTest(handle, xpos, ypos):
	"""Routine to test whatever the latest version of the window
	   code is. Does nothing clever really"""
	# get the window from a yaml file and this returns a list of indexes
	# incase we import more than 1 window we get their indexes
	# here we have 1 window so we get the first index
	index = SYAML.createWindow("./data/layouts/window_test.yml")[0]
	# we have to add modal keypresses ourselves
	SGFX.gui.keyboard.addKey(K_o, killModalWindow)
	SGFX.gui.keyboard.setModalKeys(1)
	# add the window as a dirty image
	win_img = SGFX.gui.windows[index].drawWindow()
	SGFX.gui.addDirtyRect(win_img, SGFX.gui.windows[index].rect)
	return True
	
def widgetTest(handle, xpos, ypos):
	index = SYAML.createWindow("./data/layouts/widget_test.yml")[0]
	SGFX.gui.keyboard.addKey(K_q, killModalWindow)
	SGFX.gui.keyboard.addKey(K_RETURN, killModalWindow)
	SGFX.gui.keyboard.setModalKeys(2)
	# add the dirty rect details
	SGFX.gui.addDirtyRect(SGFX.gui.windows[index].drawWindow(),
		SGFX.gui.windows[index].rect)
	return True

def menuPreferences(handle, xpos, ypos):
	"""Display the user preferences window. You can only really
	   play with the music and volume settings for now"""
	index = SYAML.createWindow("./data/layouts/menu_pref.yml")[0]
	# add the new key event: o = ok
	SGFX.gui.keyboard.addKey(K_o, killModalWindow)
	SGFX.gui.keyboard.setModalKeys(1)
	# setup dirty rect stuff
	SGFX.gui.addDirtyRect(SGFX.gui.windows[index].drawWindow(),
		SGFX.gui.windows[index].rect)
	return True

def startGame(handle, xpos, ypos):
	"""Called when user starts game"""
	# load the main scenarion
	# we call the load routine to load all the other data from 
	SGFX.gui.displayLoadingScreen(WC.SCREEN_WIDTH, WC.SCREEN_HEIGHT)
	SDATA.data.loadScenario(WC.SCENARIO)
	# delete the loading window
	killModalWindow(0, 0, 0)
	# update screen
	SGFX.gui.updateGUI()
	###############################
	# TODO: Change lore to look   #
	# like more to the game       #
	###############################
	#lore(None, None, None)
	return True
	
def welcomeScreen(handle, xpos, ypos):
	"""Routine displays opening screen, with load/save/new/about
	   buttons. Always returns True after doing it's work"""
	index = SYAML.createWindow("./data/layouts/welcome.yml")[0]
	# add the modal key events: n=new, l=load, o=options, a=about, q=quit
	SGFX.gui.keyboard.addKey(K_n, startGame)
	SGFX.gui.keyboard.addKey(K_l, menuLoad)
	SGFX.gui.keyboard.addKey(K_o, menuPreferences)
	SGFX.gui.keyboard.addKey(K_a, menuHelpAbout)
	SGFX.gui.keyboard.addKey(K_q, quitWC)
	SGFX.gui.keyboard.setModalKeys(5)
	# add the window as a dirty image
	SGFX.gui.addDirtyRect(SGFX.gui.windows[index].drawWindow(),
		SGFX.gui.windows[index].rect)
	return True

#########################################
# right click menu functions example
#########################################
def onMenu(handle, xpos, ypos):
	"""Test menu for right click"""
	# We initiate the menu
	menu = SMENU.CMenuParent(handle.describe)
	menu.addChild(SMENU.CMenuChild("New Game", "new", "Ctrl+N", menuNew))
	menu.addChild(SMENU.CMenuChild("Load Game", "open", "Ctrl+L", menuLoad))
	menu.addChild(SMENU.CMenuChild("Save Game", "save", "Ctrl+S", menuSave))
	menu.addChild(SMENU.CMenuChild("Exit HOWC", "exit", "Ctrl+Q", quitWC))
	menu.offsets = []
	menu.rect.x = handle.rect.x + handle.parent.rect.x + xpos
	menu.rect.y = handle.rect.y + handle.parent.rect.y + ypos
	SMENU.rclkMenu(menu)
	return True

#########################################	
# functions for the setup.py
#########################################

def saveSetup(handle, x, y):
	# making the list for saving
	var = []
	res = []
	for i in SGFX.gui.windows[0].items:
		if i.describe == "music":
			var.append(i.status)
	for i in SGFX.gui.windows[0].items:
		if i.describe == "intro":
			var.append(i.status)
	for i in SGFX.gui.windows[0].items:
		if i.describe == "opt-Resolution":
			if i.option == "Fullscreen":
				var.append(True)
				var.append(WC.SCREEN_WIDTH)
				var.append(WC.SCREEN_HEIGHT)
			else:
				var.append(False)
				res += i.option.split("x")
				var.append(res[0])
				var.append(res[1])
	FILE = open("./data/misc/setup.yml", 'w')
	FILE.write("# YAML setup data from setup.py\n")
	FILE.writelines(yaml.dump(var, default_flow_style=False))
	FILE.close()
	sys.exit(True)

#########################################
# functions example for dialog use
# TODO : change dialog to Class obj
#########################################

def lore(handle, x, y):
	"""Function that loads dialogs and display them """
	# open the file
	dialog = SYAML.getDialog("./data/scenario/main/lore.yml")
	SGFX.gui.story("papyrus", "Wellcome", dialog, -1, -1)
	return True

#########################################
# functions example for GUI buttons
#########################################

def enterBoard(handle, x, y):
	"""Function that loads dialogs and display them """
	# enter the system
	if SGFX.gui.current_highlight_region != None:
		SGFX.gui.switchMap(SGFX.gui.current_highlight_region)
	return True

def endTurn(handle, x, y):
	""" Function that ends the turn """
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], "End Turn", "End Turn"))
	return True
	
def reSearch(handle, x, y):
	""" Function that displays research data """
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], "reSearch", "reSearch"))
	return True
	
def exitBoard(handle, x, y):
	"""Function that loads dialogs and display them """
	# exit the board
	SGFX.gui.switchMap(None)
	# clear dirty highlighted hexes and movement highlights
	SGFX.gui.hex_dirty = []
	SGFX.gui.board_move_list = None
	return True

#########################################
# place all debugging events below this line
# these are only activated if DEBUG_MODE is set to True
#########################################

def dclickTest(handle, xpos, ypos):
	"""Test routine to check double-clicks"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], "Double-clicked!", "Test"))
	return True

def displaySliderContents(handle, xpos, ypos):
	"""As well as showing you how to read slider contents,
	   this function outputs the slider value to the terminal.
	   Sometimes useful for checking/debugging. You can add
	   it to any slider function callback"""
	# only if debugging is turned on, of course...
	if WC.DEBUG_MODE:
		print "Slider value:", handle.getSliderValue()
	return True

#########################################
# Game events
#########################################

def quitWC(handle, xpos, ypos):
	"""Messagebox to confirm quit game.
		 Returns True or doesn't return at all!"""
	SGFX.gui.addWindow(SWINDOW.CMsgbox((WC.BUTTON_OK|WC.BUTTON_CANCEL),
		[quitWC_ok, msgbox_nop],"Really quit?", "Quit Message"))
	return True
	
def quitWC_ok(handle, xpos, ypos):
	sys.exit(True)
	
def msgbox_nop(handle, xpos, ypos):
	killModalWindow(None, None, None)
	
#########################################
# Story (Lore) key events
# - TODO - this must change when story
# function becomes a widget
#########################################

def msgboxOK(handle, xpos, ypos): 
	"""Callback for messagebox ok button"""
	SGFX.gui.callback_temp = WC.BUTTON_OK
	return WC.BUTTON_OK

def msgboxCancel(handle, xpos, ypos):
	"""Callback for messagebox cancel button"""
	SGFX.gui.callback_temp = WC.BUTTON_CANCEL
	return WC.BUTTON_CANCEL

def msgboxYes(handle, xpos, ypos):
	"""Callback for messagebox yes button"""
	SGFX.gui.callback_temp = WC.BUTTON_YES
	return WC.BUTTON_YES

def msgboxNo(handle, xpos, ypos):
	"""Callback for messagebox no button"""
	SGFX.gui.callback_temp = WC.BUTTON_NO
	return WC.BUTTON_NO

def msgboxQuit(handle, xpos, ypos):
	"""Callback for messagebox quit button"""
	SGFX.gui.callback_temp = WC.BUTTON_QUIT
	return WC.BUTTON_QUIT

def msgboxIgnore(handle, xpos, ypos):
	"""Callback for messagebox ignore button"""
	SGFX.gui.callback_temp = WC.BUTTON_IGNORE
	return WC.BUTTON_IGNORE
	
#########################################
# Test and Maptest Window events
#########################################

def widget_return(handle, xpos, ypos):
	# delete the loading window
	killModalWindow(0, 0, 0)
	# update screen
	SGFX.gui.updateGUI()

def loadLayout(handle, xpos, ypos):
	""" Loads an yml to create a window for testing """
	loadWindow("./data/layouts/input_window.yml")
	
def loadWindow(filename):
	""" Loads input_window for filename input """
	# Load a text window to load the file
	index = SYAML.createWindow(filename)[0]
	# we have to add modal keypresses ourselves
	SGFX.gui.keyboard.addKey(K_RETURN, keyLoadWindow)
	SGFX.gui.keyboard.setModalKeys(1)
	# add the window as a dirty image
	SGFX.gui.addDirtyRect(SGFX.gui.windows[index].drawWindow(),
		SGFX.gui.windows[index].rect)
	# update screen
	SGFX.gui.updateOverlayWindow()
	
def keyLoadWindow(handle, xpos, ypos):
	""" LoadWindow() keyboard call """
	SGFX.gui.windows[-1].items[2].callbacks.mouse_lclk(SGFX.gui.windows[-1].items[2],0,0)
	
def loadFileWindow(handle, xpos, ypos):
	""" Loads the file selected from Ctext input """
	# delete the loading window
	killModalWindow(0, 0, 0)
	# Create the window
	filename = "./data/layouts/" + handle.parent.items[1].output + ".yml"
	if os.path.isfile(filename):
		index = SYAML.createWindow(filename)[0]
		# add the window as a dirty image
		SGFX.gui.addDirtyRect(SGFX.gui.windows[index].drawWindow(),
			SGFX.gui.windows[index].rect)
		# update screen
		SGFX.gui.updateOverlayWindow()
	else:
		SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], filename + " does not exist!", "File Error"))
	
def loadMap(handle, xpos, ypos):
	""" Loads an yml to create a map for testing """
	loadWindow("./data/layouts/input_map.yml")
	
def loadFileMap(handle, xpos, ypos):
	""" Loads the selected Map from Ctext input """
	# delete the loading window
	killModalWindow(0, 0, 0)
	# Load map
	filename = "./gfx/map/" + handle.parent.items[1].output + ".png"
	if os.path.isfile(filename):
		SGFX.gui.images["map"] = pygame.image.load(filename).convert_alpha()
		SGFX.gui.images["board"] = SGFX.gui.hex_grid_draw(SGFX.gui.images["map"], WC.COL_RED, SDATA.data.map.regions)
	else:
		SGFX.gui.addWindow(SWINDOW.CMsgbox(WC.BUTTON_OK, [msgbox_nop], filename + " does not exist!", "File Error"))
