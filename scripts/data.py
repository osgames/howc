# Source Code 2010-2011. The HOWC Team

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import pygame, sys, yaml, random
import networkx as nx
from math import floor
import defines as WC
import events as SEVENT
import gui as SGFX
import hexagon as SHEX

# Definitions for the map, players and units
# Data classes

class CPlayer(object):
	""" class tha holds all the player data """
	def __init__(self, name, race, faction):
		self.name = name
		self.race = race
		self.faction = faction
		self.colour = None
		self.units = []
		self.allies = []
		self.netrual = []
		self.hostile = []

class CHeroe(object):
	""" Class that hold heroes data """
	def __init__(self, name, stats, race, ship, image, navtype):
		self.name = name
		self.race = race
		self.stats =stats
		self.navtype = navtype
		self.ship = ship
		self.image = image
		
class CSprite(object):
	""" Class with sprite data and methods """
	def __init__(self, rect):
		# destination
		self.rect = rect
		self.surface = pygame.Surface((0,0))
		# animation definitions
		self.movement = 0
		# i/o definitions
		self.hint = None
	
	def draw(self, surface):
		surface.blit(self.image, self.rect)
		
class CWeapon(object):
	""" Class to hold weapon data """
	def __init__(self, image, power, erange):
		self.image = image
		self.power = power
		self.erange = erange

class CUnit(CSprite):
	""" Class to hold Unit data """
	def __init__(self, name, owner, image, navtype, stats, nav, heroe = None):
		CSprite.__init__(self, pygame.Rect(0, 0, WC.UNIT_WIDTH, WC.UNIT_HEIGHT))
		self.name = name
		self.owner = owner
		# type of unit
		self.navtype = navtype
		# stats of the unit
		self.movement = int(stats[0])
		self.attack = int(stats[1])
		self.defence = int(stats[2])
		self.morale = int(stats[3])
		self.luck = int(stats[4])
		self.rank = int(stats[5])
		self.hull = 0
		# effective range of weapons
		self.erange = 0
		self.heroe = heroe
		# TODO get weight from unit stats
		self.weight = 1
		# the gui image
		self.image = SGFX.gui.images[image]
		# parent navpoint
		self.nav = nav
			
	def action(self, unit):
		""" Returns availiable action agianst a target """
		print unit.name
		
	def printStats(self):
		""" Print unit stats """
		print self.name
		print self.rect
		print " Movement: " + str(self.movement)
		print " Attack: " + str(self.attack)
		print " Defence: " + str(self.defence)
		print " Morale: " + str(self.morale)
		print " Luck: " + str(self.luck)
		print " Rank: " + str(self.rank)
		print " Nav: " + str(self.nav.grid())
		print " Weight: " + str(self.weight)
	
	def drawinHex(self, surface):
		""" functions that draws the sprite onto the hex """
		self.rect.x = self.nav.rect.x + (self.nav.region.width - WC.UNIT_WIDTH)/2
		self.rect.y = self.nav.rect.y + (self.nav.region.height - WC.UNIT_HEIGHT)/2
		self.draw(surface)
			
class CInfo(object):
	def __init__(self):
		# Here we store the players
		self.players = {}
		# Here we store availiable heroes
		self.heroes = []
		# Here we store the rest of the unit types
		self.unitstype = []
				
	def loadScenario(self, scenario):
		""" Load Scenario Function """
		# Load General Info (here we will change the default defines according to scenario needs)
		gen = yaml.load(open("./data/scenario/" + scenario + "/general.yml"))
		# Load the main map
		SGFX.gui.images["map"] = pygame.image.load("./gfx/map/" + gen["map"] + ".png").convert_alpha()
		# Load Map buffer for flip()
		SGFX.gui.images["buffer"] = SGFX.gui.images["map"].copy()
		
		# Load the scenario Players
		var = yaml.load(open("./data/scenario/" + scenario + "/players.yml"))
		for i in var:
			self.players[i["player"]] = CPlayer(i["player"], i["race"], i["faction"])
			self.players[i["player"]].colour = pygame.Color(int(i["r"]), int(i["g"]), int(i["b"]))
		# Load units for the scenario
		units = yaml.load(open("./data/scenario/" + scenario + "/units.yml"))
		# Creating the heroes
		if WC.DEBUG_MODE == True: myFile = open('./units_heroes.log', 'a')
		for i in units:
			if WC.DEBUG_MODE == True: myFile.write(str(i) + "\n")
			if i.has_key("heroe") == True:
				self.heroes.append(CHeroe(i["name"], i["stats"], i["heroe"]["race"], i["image"], i["heroe"]["pic"], i["navtype"]))
			else:
				self.unitstype.append(i)
		if WC.DEBUG_MODE == True: myFile.close()
		
		# Load maps and navpoints
		self.map = CMap(scenario)
		# store a rect of the maximum map limits we can scroll to
		# obviously 0/0 for top left corner - this just denotes bottom right corner
		SGFX.gui.map_max_x = SGFX.gui.iWidth("map") - WC.SCREEN_WIDTH
		SGFX.gui.map_max_y = SGFX.gui.iHeight("map") - SGFX.gui.map_rect.h
		# Load the Mini map
		SGFX.gui.loadMiniMap("./gfx/map/" + gen["smallmap"] + ".png")
		# center map
		SGFX.gui.centerMap(int(gen["startx"]), int(gen["starty"]))
		
		# Load units
		var = yaml.load(open("./data/scenario/" + scenario + "/board.yml"))
		for v in var:
			# for every unit create a class object
			for r in v["board"]:
				# find the navpoint of the given cell X,Y
				navp = self.map.regions[v["region"]].navs[r["pxpy"][0] + r["pxpy"][1]*self.map.regions[v["region"]].hex_cols]
				# get the unit type attributes
				utype = self.returnUnitType(r["type"])
				# create the unit class
				navp.unit = CUnit(r["name"], self.players[r["owner"]], utype[0], utype[1], utype[2], navp)
				# link the unit to the player
				self.players[r["owner"]].units.append(navp.unit)
				
	def returnRandomHero(self):
		""" Function that returns a random hero and puts him out of the list """
		if len(self.heroes.keys())>0:
			name = random.choice(self.heroes.keys())
			hero = self.heroes[name]
			return name, hero
		else:
			print "[HOWC]: Error: No more heroes"
			return False
			
	def returnUnitType(self, utype):
		""" Function that returns the general attributes of a given unit type """
		for i in self.unitstype:
			if utype == i["name"]:
				return [i["image"], i["navtype"], i["stats"]]

# Map classes

class CNavpoint(object):
	""" A class that contains the constant map items """
	def __init__(self, x, y, rect, region):
		self.cellX = x
		self.cellY = y
		self.rect = rect
		self.unit = None
		self.region = region
		
	def edges(self, nx):
		""" return the neightbors of the navpoint """
		ls = nx.neighbors(self)
		return [(i.cellX, i.cellY) for i in ls]
		
	def grid(self):
		return self.cellX, self.cellY
		
class CMap(object):
	""" Class tha holds all the Main Map DATA """
	def __init__(self, scenario):
		self.regions = {}
		self.graph = nx.Graph()
		self.loadRegions(scenario)
			
	def loadRegions(self, scenario):
		""" Loads the regions from a yml file and creates the regions and networkx graph """
		# Load the map's regions from a file
		var = yaml.load(open("./data/scenario/" + scenario + "/map.yml"))
		# Make a temp list with the borders
		wlist=[]
		# for every region we will import the specific data
		for i in var:
			# we will add now a list with the connecting regions
			for region in i["borders"]:
				wlist.append((i["name"], region["name"]))
			# Append the temp list to our data
			colour = data.players[i["owner"]].colour
			self.regions[i["name"]] = CRegion(i["name"], i["xpos"], i["ypos"], i["owner"],
				colour,pygame.Rect(i["xpos"], i["ypos"], WC.REGION_SIZE, WC.REGION_SIZE))
			self.regions[i["name"]].loadNavpoints()
			self.graph.add_node(self.regions[i["name"]])
		# repeat again for graph connections
		for node in wlist:
			self.graph.add_edge(self.regions[node[0]], self.regions[node[1]])
	
	def getNeighbors(self, region, as_region = False):
		# The as_region flag determines if the region is an object(True) or a variable(False)
		if as_region:
			return [i for i in self.graph.neighbors(self.regions[region])]
		else:
			return [i.image for i in self.graph.neighbors(self.regions[region])]

	def getRetreatNeighbors(self, region):
		"""Return a list of all regions the unit can retreat to"""
		possibles = []
		for r in self.getNeighbors(region.image, True):
			if r.owner == region.owner and len(r.units) != WC.MAX_STACKING:
				possibles.append(region)
		return possibles

class CRegion(SHEX.CHexagon):
	""" Class that holds region data for the main map """
	def __init__(self, name, x, y, owner, colour, rect, mask = "regmask"):
		SHEX.CHexagon.__init__(self,WC.HEX_SIDE)
		# image and mask is a str
		self.name = name
		self.graph = nx.Graph()
		self.rect = rect
		self.navs = []
		self.owner = owner
		self.colour = colour
		self.cellX = x
		self.cellY = y
		# check if this is a region/poi or hexagon and define the rectangle
		self.sysmap = return_map(name)
		# calculating hex grid for the main map and offsets
		self.hex_cols = 0
		self.hex_rows = 0
		self.map_w_offset = 0
		self.map_h_offset = 0
		# If the region doesn't have hex grid
		self.mask = mask
		
	def loadNavpoints(self):
		""" Creating the navpoints inside a hexagon grid system for the region """
		self.hex_cols = int((SGFX.gui.images[self.sysmap].get_width() - self.odd_row_offset) / self.cell_width)
		self.hex_rows = int((SGFX.gui.images[self.sysmap].get_height() - self._h) / self.cell_height)
		self.map_w_offset = (SGFX.gui.images[self.sysmap].get_width() - self.odd_row_offset - self.hex_cols * self.cell_width) / 2
		self.map_h_offset = (SGFX.gui.images[self.sysmap].get_height() - self._h - self.hex_rows * self.cell_height) / 2
		# Creating all the navs ( hexes or region/poi ) and offsets variables
		if WC.DEBUG_MODE == True:
			myFile = open('./make_board_region.log', 'a')
			myFile.write("---------- CREGION ----------\n")
			myFile.write(str(self.cellX) + "x" + str(self.cellY) + "\n")
		for y in range(self.hex_rows):
			for x in range(self.hex_cols):
				# Get the top left location of the tile.
				pixelX,pixelY = self.hexMap2Pixel(x,y)
				pixelX += 1 # offset to allow for anti-aliasing
				pixelY += 1
				node = CNavpoint(x, y, pygame.Rect(pixelX, pixelY, self.width, self.height), self)
				if WC.DEBUG_MODE == True:
					myFile.write("NAV: " + str(x) + "x" + str(y) + "\n")
				# horizontal edge
				if self.hex_cols >= x > 0: self.graph.add_edge(self.navs[-1], node)
				# vertical edge
				if y > 0:
					self.graph.add_edge(self.navs[-self.hex_cols], node)
					if (y % 2) == 0 and x > 0:
						self.graph.add_edge(self.navs[-self.hex_cols], node)
						self.graph.add_edge(self.navs[-1-self.hex_cols], node)
					elif (y % 2) == 1 and x < (self.hex_cols -1):
						self.graph.add_edge(self.navs[1 - self.hex_cols], node)
						self.graph.add_edge(self.navs[-self.hex_cols], node)
				# Add the last node to the graph
				self.graph.add_node(node)
				self.navs.append(node)
		# TODO: Load the navpoints information
		if WC.DEBUG_MODE == True: myFile.close()
		
	def changeOwner(self, new_owner, colour):
		self.owner = new_owner
		self.colour = colour
		
	def changeWeight(self, nav, weight):
		""" Change the weight of the edges ( connections between nav points ) for pathfinding """
		neighbors = self.getNeighbors(nav)
		for n in neighbors: self.graph[nav][n]['weight'] = weight
			
	def getNeighbors(self, nav):
		""" Rerurns the hexagon neighbors """
		return [i for i in self.graph.neighbors(nav)]
	
	def getRange(self, nav, movement):
		""" Returns the hexagons in the movement distance """
		return nx.single_source_shortest_path_length(self.graph, nav, movement)
		
	def getPath(self, navA, navB):
		""" Returns the path between two nav points """
		return True
		
	def aStarDist(self, a, b):
		""" Returns the distance between two nav points for A star algorithm """
		# Usage: nx.astar_path(G,(0,0),(2,2),region.aStarDist)
		(x1, y1) = a
		(x2, y2) = b
		return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5
	
	def moveUnit(self, source, destination):
		""" move a unit inside a region and set the animation on motion
			source and destination are the from and to regions """
		if destination.unit == None:
			destination.unit = source.unit
			source.unit = None
			destination.unit.nav = destination
			self.changeWeight(source, 1)
			self.changeWeight(destination, destination.unit.weight)
			return True
		else: return False

# general functions for hex and map handling

def return_map(system):
	""" this is a procedural method to define the same map
		to the same text """
	bar=0
	for i in system: bar+=ord(i)
	return WC.GFX_MAP[bar%len(WC.GFX_MAP)]
	
def iterRegions():
	"""A custom iterator so we can change how regions are held"""
	for i in data.map.regions.itervalues():
		yield i

def regionClicked(x, y):
	"""Return name of region if clicked, or False"""
	for i in iterRegions():
		if i.rect.collidepoint(x, y):
			# now just check against the mask
			nx = x - i.rect.x
			ny = y - i.rect.y
			if SGFX.gui.images["regmask"].get_at((nx, ny))[3] != 0:
				return i
	return None
	
def hexRegionClicked(x, y):
	"""Return the hexagon of the region if clicked, or False"""
	mapX, mapY = SGFX.gui.current_highlight_region.pixel2HexMap(x,y)
	# check if this is inside the map
	for i in SGFX.gui.current_highlight_region.navs:
		if i.cellX == mapX and i.cellY == mapY:
			if 0 <= mapX <= SGFX.gui.current_highlight_region.hex_cols:
				if 0 <= mapY <= SGFX.gui.current_highlight_region.hex_rows: return i
	return None

def getRegion(region):
	return data.map.regions[region]

def getNeighbors(region):
	return data.map.getNeighbors(region)
	
def getActionList(first, second):
	""" return a list of availiable action for the specific unit type """
	### IMPORTANT ###
	# THIS IS A TEMP FUNCTION UNTIL ALL UNIT PROPERTIES IS IN PLACE, WEAPONS ETC
	#################
	# list the eligible actions for the second unit ( This simulates what can happen to a ship )
	if second in ["corvette", "dreadnought", "submarine", "transport", "carrier", "battleship", "cruiser", "destroyer", "frigate"]: f = [WC.ATTACK, WC.BOARD]
	elif second in ["fighter", "bomber", "shuttle"]: f = [WC.ATTACK]
	elif second == "planet": f = [WC.BOARD, WC.BOMBARD, WC.LAND]
	elif second == "buoy": f = [WC.JUMP]
	elif second in ["minebase", "starbase", "refinerybase", "shipyard", "fort"]: f = [WC.ATTACK, WC.BOARD, WC.LAND]
	elif second == "asteroid": f = [WC.ATTACK, WC.MINE]
	# else is for heroe actions
	else: f = [WC.ATTACK]
	# remove not eligible action for first unit from the list ( This simulates what a ship can do )
	if first in ["planet", "minebase", "starbase", "refinerybase", "shipyard", "fort"]: s = [WC.ATTACK, WC.SCRAMBLE]
	elif first in ["corvette", "submarine", "transport", "carrier", "destroyer", "frigate"]: s = [WC.ATTACK, WC.SCRAMBLE, WC.BOARD, WC.JUMP, WC.LAND]
	elif first in ["dreadnought", "battleship", "cruiser"]: s = [WC.ATTACK, WC.SCRAMBLE, WC.BOARD, WC.BOMBARD, WC.JUMP, WC.LAND]
	elif first in ["fighter", "bomber", "shuttle"]: s = [WC.ATTACK, WC.JUMP, WC.LAND]
	ac = []
	for i in s:
		if i in f: ac.append(i)
	return ac
		
data = CInfo()
