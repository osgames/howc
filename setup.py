#!python.exe
import site

#!/usr/bin/python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# setup code for game
# for now, you can only set the game resolution

import sys, pygame, yaml, os
from pygame.locals import *

from scripts import data as SDATA
from scripts import defines as WC
from scripts import gui as SGFX
from scripts import window as SWINDOW
from scripts import widgets as SWIDGET
from scripts import ybuild as SYAML
from scripts import events as SEVENT

if __name__ == "__main__":
	""" Main function for the setup/options application """
	WC.SCREEN_WIDTH = 285
	WC.SCREEN_HEIGHT = 192
	SGFX.gui.mainInit(WC.GRAPHICS_F)
	SYAML.createWindow("./data/layouts/setup_window.yml")
	SEVENT.quitWC = SEVENT.quitWC_ok
	var = yaml.load(open("./data/misc/setup.yml"))
	music = var[0]
	intro = var[1]
	fullscreen = var[2]
	width = int(var[3])
	height = int(var[4])
	for i in SGFX.gui.windows[0].items:
		if i.describe == "opt-Resolution":
			if fullscreen == True:
				i.option = "Fullscreen"
			else:
				i.option = str(width)+"x"+str(height)
			i.upd()
		elif i.describe == "music":
			i.status = music
			i.upd(None, None, None)
		elif i.describe == "intro":
			i.status = intro
			i.upd(None, None, None)
	SGFX.gui.updateGUI()
	SGFX.gui.mainLoop()
