#!python.exe
import site

# Source Code 2010-2011. The HOWC Team

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import sys, pygame, yaml
from pygame.locals import *
from scripts import defines as WC
from scripts import data as SDATA
from scripts import gui as SGFX
from scripts import window as SWINDOW
from scripts import widgets as SWIDGET
from scripts import menu as SMENU
from scripts import events as SEVENT

class CGame(object):
	def __init__(self):
		# Load the default values from setup.yml config file. In DEBUG MODE the default values are in defines.py
		if WC.DEBUG_MODE == False:
			var = yaml.load(open("./data/misc/setup.yml"))
			WC.MUSIC_ON = var[0]
			WC.INTRO = var[1]
			WC.FULLSCREEN = var[2]
			WC.SCREEN_WIDTH = int(var[3])
			WC.SCREEN_HEIGHT = int(var[4])
		WC.LOADSCREEN = True
		# init the data
		self.sortOptions()
		SGFX.gui.mainInit(["gui", "icons", "units", "units/overlays", "npc" , "nav", "Blueprints", "anim", "map"])

	def executeFlag(self, flag):
		"""Call with flag, which should be a single letter.
		   Routine does whatever has to be done. Returns
		   True if something happened, false otherwise"""
		if flag == 'v':
			# show version detail s and exit
			print "HOWC " + WC.VERSION + ", written and designed by Chris Stamoultas"
			print "  Copr. 2010-2011, released under the GNU Public License v3"
			sys.exit(True)
		elif flag == "?":
			print "HOWC Command line options:"
			print "  -v : Show version details"
			print "  -? : Show this display"
			# all done, a valid exit point
			sys.exit(True)
		# no match, but not a terrible error
		print "[HOWC] Error: No option for flag " + flag
		return False
	
	def sortOptions(self):
		"""Routine reads command-line options. Returns False if any problem"""
		options = sys.argv
		# any args at all?
		if len(options) == 1:
			return True
		# remove the first one
		options.pop(0)
		for i in options:
			# split into characters:
			chars = list(i)
			# first character must be a '-'
			if chars[0] != '-':
				print "[HOWC]: Error: Flag sequence does not start with '-'"
				sys.exit(False)
			# remove it
			chars.pop(0)
			# anything left?
			if len(chars) == 0:
				print "[HOWC]: Error: No flags following '-' char"
				return False
			# do each flag
			for flag in chars:		
				self.executeFlag(flag)
		# everything must have worked
		return True

	# routine to init everything...
	def setupStart(self):
		"""Set up the howc gui; keychecks, windows, and menus"""
		# There are 3 parts to the interface: the menu bar at the top - 
		# DO NOT add widgets here, only the menu and the version number info
		# are held here; The main map and then the widgets on the main map.
		# we make a seperate blank window to hold all of these widgets
		menu = []
		menu.append(SMENU.CMenuParent("File"))
		if WC.DEBUG_MODE:
			menu.append(SMENU.CMenuParent("Debug"))
		# then add the sub menus below these
		menu[0].addChild(SMENU.CMenuChild("New Game", "new", "Ctrl+N", SEVENT.menuNew))
		menu[0].addChild(SMENU.CMenuChild("sep", None, "", SEVENT.notYetCoded))
		menu[0].addChild(SMENU.CMenuChild("Load Game", "open", "Ctrl+L", SEVENT.menuLoad))
		menu[0].addChild(SMENU.CMenuChild("Save Game", "save", "Ctrl+S", SEVENT.menuSave))
		# this is a seperate, drawn bar to split the text
		menu[0].addChild(SMENU.CMenuChild("sep", None, "", SEVENT.notYetCoded))
		menu[0].addChild(SMENU.CMenuChild("Preferences", "preferences", "Ctrl+P", SEVENT.menuPreferences))
		menu[0].addChild(SMENU.CMenuChild("sep", None, "", SEVENT.notYetCoded))
		menu[0].addChild(SMENU.CMenuChild("Exit HOWC", "exit", "Ctrl+Q", SEVENT.quitWC))
		# debug menu is always last - it's easy to remove then
		if WC.DEBUG_MODE:
			menu[1].addChild(SMENU.CMenuChild("Window test", "debug", "", SEVENT.windowTest))
			menu[1].addChild(SMENU.CMenuChild("Widget test", "debug", "", SEVENT.widgetTest))
			menu[1].addChild(SMENU.CMenuChild("Dialog test", "debug", "", SEVENT.lore))
	
		# Add the menubar at the top. It has no drawn window
		# THIS MUST BE THE FIRST WINDOW
		index = SGFX.gui.addWindow(SWINDOW.CWindow(0, 0, 0, 0, "", False, describe="Menus"))
		SGFX.gui.windows[index].border_offset = False
		
		################ CMenu widget ####################
		# Add the prepared menu or an empty menu bar
		##################################################
		#SGFX.gui.windows[index].addWidget(SMENU.CMenu(menu))
		
		# add an empty menu bar for later use
		# we use this insteed the SMENU.CMenu(menu) widget.
		# IMPORTANT: we must not use both at the same time
		rect = pygame.Rect(0,0,WC.SCREEN_WIDTH,SGFX.gui.iHeight("titlebar"))
		rhs_txt = "HOWC " + WC.VERSION
		SGFX.gui.windows[index].addWidget(SWIDGET.CImage(rect.x, rect.y, rect.w, rect.h, None))
		SGFX.gui.windows[index].items[-1].image.blit(SMENU.drawMenuBar(rect, rhs_txt), (0, 0))
		#-------------------------------------------------
		
		# bliting only the first window for now which is the menu bar.
		if SGFX.gui.windows[0].display:
			SGFX.gui.screen.blit(SGFX.gui.windows[0].image, 
					(SGFX.gui.windows[0].rect.x, SGFX.gui.windows[0].rect.y))
		for item in SGFX.gui.windows[0].items:
			if item.visible:
				x1 = SGFX.gui.windows[0].rect.x + item.rect.x
				y1 = SGFX.gui.windows[0].rect.y + item.rect.y
				SGFX.gui.screen.blit(item.image, (x1, y1))
		pygame.display.flip()
		
		# adds a window with no frame that holds the GUI widgets to overlay on the map
		# THIS IS ALWAYS THE SECOND WINDOW!
		self.addGUIelements()
		
		# add all the keys for the gui. True if there if a menu bar or false if there is not one.
		self.addKeys(False)

		# finally, the last thing we do is start the animation timer
		pygame.time.set_timer(WC.EVENT_ANIM, WC.ANIM_TIME)
		# display the welcome screen.
		SEVENT.welcomeScreen(0, 0, 0)
	
	def addGUIelements(self):
		""" Here we configure how our GUI will look like
			and we return a window that contains that widgets. """
		# Make a window with no frame that holds the GUI widgets
		bwindow = SWINDOW.CWindow(0, SGFX.gui.iHeight("titlebar"), WC.SCREEN_WIDTH,
								  WC.SCREEN_HEIGHT - SGFX.gui.iHeight("titlebar"), "",
								  False, "map_widgets")
						  
		######## CImage mini-map widget ##################
		# This is an image widget that acts like a mini-map on the rhs
		##################################################
		w = WC.SMALL_MAP_W
		h = WC.SMALL_MAP_H
		x = WC.SCREEN_WIDTH - (WC.SMALL_MAP_W + WC.SPACER + bwindow.rect.x)
		y = WC.SCREEN_HEIGHT - (WC.SMALL_MAP_H + (2*WC.SPACER) + 1 + bwindow.rect.y)
		mini_map = SWIDGET.CImage(x, y, w, h, "small_map")
		# allow left mouse button dragging as well (also simulates a mini-map click)
		mini_map.callbacks.mouse_ldown = SEVENT.miniMapDrag
		mini_map.active = True
		mini_map.describe = "mini-map"
		bwindow.addWidget(mini_map)
		# we need to store it as well
		SGFX.gui.minimap_widget = mini_map
		#-------------------------------------------------
		
		######## CImage unit info box widget #############
		# This is an image widget that acts like a mini-map on the rhs
		##################################################
		w = WC.SMALL_MAP_W
		h = WC.SMALL_MAP_H
		x = WC.SPACER + bwindow.rect.x
		y = WC.SCREEN_HEIGHT - (WC.SMALL_MAP_H + (2*WC.SPACER) + 1 + bwindow.rect.y)
		info_widget = SWIDGET.CImage(x, y, w, h, "small_map")
		# allow left mouse button dragging as well (also simulates a mini-map click)
		#info_widget.callbacks.mouse_ldown = SEVENT.info_widget
		info_widget.active = False
		info_widget.visible = False
		info_widget.describe = "info_widget"
		bwindow.addWidget(info_widget)
		# we need to store it as well
		SGFX.gui.info_widget = info_widget
		#-------------------------------------------------
		
		########   CHightlight system map area   #########
		# This is a highlighted area for the board widgets
		##################################################
		w = (WC.SYS_WIDGETS * WC.SYS_SIZE) + ((WC.SYS_WIDGETS+1) * WC.SPACER)
		h = WC.SYS_SIZE + 10
		x = WC.SCREEN_WIDTH - w - WC.SPACER
		y = WC.SPACER
		sw = SWIDGET.CHightlight(x, y, w, h, WC.T_COLG_BLUE)
		sw.visible = True
		sw.active = False
		SGFX.gui.main_map_widgets.append(sw)
		bwindow.addWidget(sw)
		#-------------------------------------------------
		
		########### CImage End turn widget ###############
		# This is an image widget that acts like a button
		##################################################
		sw1 = SWIDGET.CImage(x + WC.SPACER, y + 5, SGFX.gui.iWidth("house"), SGFX.gui.iHeight("house"),"house")
		sw1.describe = "End Turn"
		sw1.visible = True
		sw1.active = True
		sw1.callbacks.mouse_lclk = SEVENT.endTurn
		#sw1.callbacks.mouse_over = SEVENT.hint
		SGFX.gui.main_map_widgets.append(sw1)
		bwindow.addWidget(sw1)
		#-------------------------------------------------
		
		######## CImage Research button widget ###########
		# This is an image widget that acts like a button
		##################################################
		sw2 = SWIDGET.CImage(x + SGFX.gui.iWidth("research") + (2 * WC.SPACER), y + 5,
								SGFX.gui.iWidth("research"), SGFX.gui.iHeight("research"),"research")
		sw2.describe = "Research"
		sw2.visible = True
		sw2.active = True
		sw2.callbacks.mouse_lclk = SEVENT.reSearch
		#sw2.callbacks.mouse_over = SEVENT.hint
		SGFX.gui.main_map_widgets.append(sw2)
		bwindow.addWidget(sw2)
		#-------------------------------------------------
		
		######## CImage Enter board map widget ###########
		# This is an image widget that acts like a button
		##################################################
		sw3 = SWIDGET.CImage(x + (2 * SGFX.gui.iWidth("find")) + (3 * WC.SPACER), y + 5,
								SGFX.gui.iWidth("find"), SGFX.gui.iHeight("find"),"find")
		sw3.describe = "Enter Region"
		sw3.visible = True
		sw3.active = True
		sw3.callbacks.mouse_lclk = SEVENT.enterBoard
		#sw3.callbacks.mouse_over = SEVENT.hint
		SGFX.gui.main_map_widgets.append(sw3)
		bwindow.addWidget(sw3)
		#-------------------------------------------------
	
		# This must be last so it wont overlap all the other widgets
		
		######## CWidget blank widget ####################
		# blank simple widget that catches all the calls for the map
		##################################################
		map_widget = SWIDGET.CWidget(pygame.Rect((0, 0, WC.SCREEN_WIDTH,
									WC.SCREEN_HEIGHT - SGFX.gui.iHeight("titlebar"))), WC.WT_MAP, None, "map_widget", 
									active = True, visible = False)
		map_widget.callbacks.mouse_lclk = SGFX.gui.mapClick
		#map_widget.callbacks.mouse_over = SGFX.gui.mapHint
		bwindow.addWidget(map_widget)
		#-------------------------------------------------
		# In case we need it to have a flag of the map widget for callbacks
		#SGFX.gui.map_widget = map_widget
		#-------------------------------------------------
		SGFX.gui.addWindow(bwindow)
		return True

	def addKeys(self, ismenu):
		"""Adds keys needed at the start of the game"""
		# n for next unit, r for rome, i.e. centre the map
		# CTRL-Q for exit, F1 for help; alt-f, alt-e and alt-h for menus
		# n - next unit turn, r - centre map on rome
		# f - finish this units turn, k - display standard keys list;  
		SGFX.gui.keyboard.addKey(K_r, SEVENT.centerMap)
		# allow map scrolling with curser keys
		SGFX.gui.keyboard.addKey(K_UP, SEVENT.keyScrollUp)
		SGFX.gui.keyboard.addKey(K_DOWN, SEVENT.keyScrollDown)
		SGFX.gui.keyboard.addKey(K_RIGHT, SEVENT.keyScrollRight)
		SGFX.gui.keyboard.addKey(K_LEFT, SEVENT.keyScrollLeft)
		if ismenu:
			# add menu shortcut keys
			SGFX.gui.keyboard.addKey(K_f, SEVENT.keyMenuFile, KMOD_LALT)
			SGFX.gui.keyboard.addKey(K_e, SEVENT.keyMenuEmpire, KMOD_LALT)
			SGFX.gui.keyboard.addKey(K_h, SEVENT.keyMenuHelp, KMOD_LALT)
			SGFX.gui.keyboard.addKey(K_n, SEVENT.menuNew, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_l, SEVENT.menuLoad, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_s, SEVENT.menuSave, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_p, SEVENT.menuPreferences, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_q, SEVENT.quitWC, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_u, SEVENT.notYetCoded, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_a, SEVENT.menuHelpAbout, KMOD_LCTRL)
			SGFX.gui.keyboard.addKey(K_F1, SEVENT.menuHelpHelp)
			SGFX.gui.keyboard.addKey(K_k, SEVENT.keyShowKeys)
			SGFX.gui.keyboard.addKey(K_n, SEVENT.notYetCoded)
			# debug menu added?
			if WC.DEBUG_MODE:
				SGFX.gui.keyboard.addKey(K_d, SEVENT.keyMenuDebug, KMOD_LALT)
				SGFX.gui.keyboard.addKey(K_ESCAPE, SEVENT.keyMenuEscape)

def main():
	"""Setup everything and then run it"""
	game = CGame()
	game.setupStart()
	# call the gui main loop
	SGFX.gui.mainLoop()

if __name__ == '__main__':
	main()

