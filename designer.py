# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Test code for game

import sys, pygame, yaml, re, os
from pygame.locals import *

from scripts import data as SDATA
from scripts import defines as WC
from scripts import gui as SGFX
from scripts import window as SWINDOW
from scripts import widgets as SWIDGET
from scripts import ybuild as SYAML
from scripts import menu as SMENU
from scripts import events as SEVENT

def addKeys():
	"""Adds keys needed at the start of the game"""
	# n for next unit, r for rome, i.e. centre the map
	# CTRL-Q for exit, F1 for help; alt-f, alt-e and alt-h for menus
	# n - next unit turn, r - centre map on rome
	# f - finish this units turn, k - display standard keys list;  
	SGFX.gui.keyboard.addKey(K_f, SEVENT.keyMenuFile, KMOD_LALT)
	SGFX.gui.keyboard.addKey(K_ESCAPE, SEVENT.keyMenuEscape)
	# add menu shortcut keys
	SGFX.gui.keyboard.addKey(K_n, SEVENT.menuNew, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_l, SEVENT.menuLoad, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_s, SEVENT.menuSave, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_p, SEVENT.menuPreferences, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_q, SEVENT.quitWC, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_u, SEVENT.notYetCoded, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_a, SEVENT.menuHelpAbout, KMOD_LCTRL)
	SGFX.gui.keyboard.addKey(K_F1, SEVENT.menuHelpHelp)
	SGFX.gui.keyboard.addKey(K_k, SEVENT.keyShowKeys)
	SGFX.gui.keyboard.addKey(K_n, SEVENT.notYetCoded)

if __name__ == "__main__":
	""" Main function for the test widget application """
	# Set the program caption for the main window
	pygame.display.set_caption("Window Designer")
	# Init gui. 
	SGFX.gui.mainInit(WC.GRAPHICS_F)
	# Add the menubar at the top. It has no drawn window
	menu = []
	menu.append(SMENU.CMenuParent("File"))
	menu.append(SMENU.CMenuParent("test"))
	# then add the sub menus below these
	menu[0].addChild(SMENU.CMenuChild("New Window", "new", "Ctrl+N", SEVENT.notYetCoded))
	menu[0].addChild(SMENU.CMenuChild("sep", None, "", SEVENT.notYetCoded))
	menu[0].addChild(SMENU.CMenuChild("Load Window", "open", "Ctrl+L", SEVENT.loadLayout))
	menu[0].addChild(SMENU.CMenuChild("Save Window", "save", "Ctrl+S", SEVENT.notYetCoded))
	# this is a seperate, drawn bar to split the text
	menu[0].addChild(SMENU.CMenuChild("sep", None, "", SEVENT.notYetCoded))
	menu[0].addChild(SMENU.CMenuChild("Exit", "exit", "Ctrl+Q", SEVENT.quitWC))
	# this is the second menu
	menu[1].addChild(SMENU.CMenuChild("Preferences", "preferences", "Ctrl+P", SEVENT.menuPreferences))
	menu[1].addChild(SMENU.CMenuChild("Window test", "debug", "", SEVENT.windowTest))
	menu[1].addChild(SMENU.CMenuChild("Widget test", "debug", "", SEVENT.widgetTest))
	menu[1].addChild(SMENU.CMenuChild("Dialog test", "debug", "", SEVENT.lore))
	
	# THIS MUST BE THE FIRST WINDOW
	index = SGFX.gui.addWindow(SWINDOW.CWindow(0, 0, 0, 0, "", False, describe="Menus"))
	SGFX.gui.windows[index].border_offset = False
	SGFX.gui.windows[index].addWidget(SMENU.CMenu(menu))
	# bliting only the first window for now which is the menu bar.
	if SGFX.gui.windows[0].display:
		SGFX.gui.screen.blit(SGFX.gui.windows[0].image, 
				(SGFX.gui.windows[0].rect.x, SGFX.gui.windows[0].rect.y))
	for item in SGFX.gui.windows[0].items:
		if item.visible:
			x1 = SGFX.gui.windows[0].rect.x + item.rect.x
			y1 = SGFX.gui.windows[0].rect.y + item.rect.y
			SGFX.gui.screen.blit(item.image, (x1, y1))
	pygame.display.flip()
	# add all the keys for the gui
	addKeys()
	SGFX.gui.updateGUI()
	SGFX.gui.mainLoop()
