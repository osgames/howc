This is not yet automated but very easy.

First download howc.tar from "http://howc.svn.sourceforge.net/viewvc/howc/"
and untar to your prefered folder, or checkout the latest revision from
"svn co https://howc.svn.sourceforge.net/svnroot/howc howc" 

From ".\howc\installation" folder install the followings in this order:

a) python-2.5.4.msi
b) pygame-1.9.1.win32-py2.5.msi
c) networkx-1.2.linux-i686.exe
d) PyYAML-3.09.win32-py2.5.exe

HOWC is ready, you must have python executables in path. You can add the easy
by giving "path c:\python25\;%PATH%" and then just run "howc.exe"

